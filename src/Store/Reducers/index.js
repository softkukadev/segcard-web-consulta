import auth from './auth'
import provider from './provider'

export default {
    auth,
    provider
}