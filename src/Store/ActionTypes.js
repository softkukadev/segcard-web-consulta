export const LOGIN = 'LOGIN'
export const LOGOUT= 'LOGOUT'
export const CHANGE_PASSWORD= 'CHANGE_PASSWORD'

export const LOAD_DEFAULT = 'LOAD_DEFAULT'
export const UNLOAD_DEFAULT = 'UNLOAD_DEFAULT'

export default {
    AUTH: {
        LOGIN,
        LOGOUT,
        CHANGE_PASSWORD
    },
    PROVIDER: {
        LOAD_DEFAULT,
        UNLOAD_DEFAULT
    }
}