import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import './App.css';
import Navigator from './Navigator';
import { Provider } from 'react-redux'
import store from './Store';
import moment from 'moment'
import 'moment/locale/pt-br'

moment.locale('pt-br')

function App() {
  return (
    <Provider store={store}>
      <Navigator />
    </Provider>
  );
}

export default App;
