import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Header} from 'semantic-ui-react'
import Api from '../../Api'
import ListaVendas from '../ListaVendas/ListaVendas'
import Estatisticas from './Estatisticas'

const NovaVenda = props => {
    const { provider, currentRole } = props
    const [refresh, setRefresh] = useState(1);
    
    async function load(){
        await Api.Provider.get_current()
    }

    function callbackNovaVenda(){
        setRefresh(refresh+1)
    }

    useEffect(() => {
        load()
    }, [])

    return (
        <>
            <Estatisticas refresh={refresh} />

            <Header as="h2" attached="top" content="Minhas ultimas transações"/>
            <ListaVendas refresh={refresh} callback={callbackNovaVenda} currentRole={currentRole} attached padded />
        </>
    )
}

const mapStateToProps = state => ({
    provider: state.provider
});
  
export default connect(mapStateToProps)(NovaVenda)