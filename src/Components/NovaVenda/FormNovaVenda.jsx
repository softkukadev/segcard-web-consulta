import React, { useState, useEffect, useCallback } from 'react'
import { Form, Grid, Header, Message } from 'semantic-ui-react'
import Payment from "payment";
import InputMask from 'react-input-mask'
import Cards from 'react-credit-cards'
import { CPF } from 'cpf_cnpj'
import { MAIN_COLOR } from '../../environment'
import Api from '../../Api'
import 'react-credit-cards/es/styles-compiled.css'
import NumberFormat from 'react-number-format';
import { isFunction } from 'util';
import ErrorHandler from '../ErrorHandler';
import Axios from 'axios';



const ESTADO_INICIAL = {
    name: '',
    cpf: '',
    sexo: '',
    UF: '',
    birthdate: '',
    card: '',
    cvv: '',
    expires: '',
    email: '',
    phone_number: '',
    endereco: {
        cep: '',
        logradouro: '',
        numero: '',
        bairro: '',
        cidade: '',
        estado: ''
    },
    amount: 37.9   
}

const STATUS_INICIAL = {
    loading: false,
    erro: false,
    sucesso: false,
    mensagem: ''
}

const optionsSexo = [
    { key: 'M', text: 'Masculino', value: 'M'},
    { key: 'F', text: 'Feminino', value: 'F'}
]

const FormNovaVenda = props => {
    const { provider, callback } = props
    const [form, setForm] = useState({...ESTADO_INICIAL})
    const [foco, setFoco] = useState('');
    const [status, setStatus] = useState({...STATUS_INICIAL})
    const [erro, setErro] = useState(null);

    useEffect(() => {
        Payment.formatCardNumber(document.getElementsByName("card")[0]);
        Payment.formatCardCVC(document.getElementsByName("cvv")[0]);
        Payment.formatCardExpiry(document.getElementsByName("expires")[0]);
    }, [])

    useEffect(() => {
        setForm((form) => ({...form, amount: provider.valor_plano}))
    }, [provider.valor_plano])

    const submit = async () => {
        const dados = {...form}
        setStatus({...STATUS_INICIAL, loading: true})
        setErro(null);
        try {
            validateData(dados)
            const sell = await Api.Sell.new_sale(provider.slug, dados);
            setStatus({...ESTADO_INICIAL, sucesso: true, mensagem: "Transação criada com sucesso"})
            setForm({...ESTADO_INICIAL})

            isFunction(callback) && callback();
        } catch(erro){
            setErro(erro.response && erro.response.data.data);
            if(provider.name === 'SK-PAYU'){
                setErro(erro.response && {...erro.response.data.transactionResponse });
            }
            if(provider.name === 'HUBSALE'){
                setErro(erro.response && {...erro.response.data.data });
            }
            if(provider.name === 'CIELO'){
                setErro(erro.response && {...erro.response.data.data, message:erro.response.data.message });
            }
            if(provider.name === 'VINDI'){
                setErro(erro.response && {...erro.response.data, message:erro.response.data });
            }
            if(provider.name === 'REDE'){
                setErro(erro.response && {...erro.response.data.data, message:erro.response.data.message });
            }
            if(erro.response && erro.response.data){
                setStatus({...ESTADO_INICIAL, erro: true, mensagem: erro.response.data.erro})
            } else {
                setStatus({...ESTADO_INICIAL, erro: true, mensagem: erro.message})
            }
        } finally{
            //setStatus({...STATUS_INICIAL, loading: false})
        }
    }

    function validateData(dados) {
        if(!(dados.card || Payment.fns.validateCardNumber(dados.card))){
            throw new Error("Número do cartão inválido")
        }
        if(provider.requires_cvv && !(dados.cvv || Payment.fns.validateCardCVC(dados.cvv))){
            throw new Error("CVV inválido")
        }
        if(!(dados.cvv || Payment.fns.validateCardExpiry(dados.expires))){
            throw new Error("Validade do cartão inválido")
        }

        if(!(dados.cpf || CPF.isValid(dados.dados.cpf))){
            throw new Error("Validade do cartão inválido")
        }
    }

    const changeField = (_, {name, value}) => {
        setForm({...form, [name]: value})
    }

    const fillAddress = useCallback(async (cep) => {
        try{
            const cepClean = cep.replace(/\D/, '');
            const { data } = await Axios.get(`https://viacep.com.br/ws/${cepClean}/json/`)
            let objEndereco = {}
            if(data.erro) {
                objEndereco = {
                    cep,
                    logradouro: '',
                    bairro: '',
                    estado: '',
                    cidade: ''
                }
                
            } else {
                objEndereco = {
                    cep,
                    logradouro: data.logradouro,
                    bairro: data.bairro,
                    estado: data.uf,
                    cidade: data.localidade
                }
            }
            setForm(form => ({...form, endereco: objEndereco}))
        } finally{

        }
    }, [])

    const changeFieldEndereco = (_, {name, value}) => {
        if(name === 'cep' && value?.match(/\d{5}-\d{3}/)){
            fillAddress(value);
        }
        setForm(form => ({...form, endereco: { ...form.endereco, [name]: value }}))
    }

    const handleFocus = (field) => () => {
        setFoco(field)
    }

    return (
        <Grid>
            <Grid.Column width={11}>
                <Form success={status.sucesso} error={status.erro} onSubmit={submit}>
                    <Header as="h4" content="Informações do cliente" icon="user outline" />
                    <Form.Group>
                        <Form.Field width={8} required>
                            <label>Nome</label>
                            <Form.Input name="name" required value={form.name} onFocus={handleFocus('name')} onChange={changeField} />
                        </Form.Field>
                        <Form.Field width={3} required>
                            <label>Data de nascimento</label>
                            <Form.Input name="birthdate" required type="date" value={form.birthdate} onChange={changeField} />
                        </Form.Field>
                        <Form.Field width={3} required>
                            <label>Sexo</label>
                            <Form.Select name="sexo" required value={form.sexo} options={optionsSexo} onChange={changeField} />
                        </Form.Field>
                        <Form.Field width={2} required>
                            <label>UF</label>
                            <Form.Input name="UF" required value={form.UF} onChange={changeField} />
                        </Form.Field>
                    </Form.Group>
                    
                    <Form.Group>
                        <Form.Field width={6} required>
                            <label>CPF</label>
                            <InputMask mask="999.999.999-99" required maskChar="_" value={form.cpf} onChange={e => changeField(e, e.target)} name="cpf" />
                        </Form.Field>
                        <Form.Field width={6} required>
                            <label>Email</label>
                            <Form.Input name="email" required value={form.email} onChange={changeField} />
                        </Form.Field>
                        <Form.Field width={6} required>
                            <label>Telefone</label>
                            <InputMask mask={form.phone_number.length < 15 ? '(99) 9999 99999': '(99) 9 9999 9999' } required maskChar={null} value={form.phone_number} onChange={e => changeField(e, e.target)} name="phone_number" />
                        </Form.Field>
                    </Form.Group>
                    
                    <Header as="h4" content="Informações de endereço" icon="map" />
                    <Form.Group>
                        <Form.Field width={2} required={provider.name==='HUBSALE'}>
                            <label>CEP</label>
                            <InputMask  required={provider.name==='HUBSALE'} maskChar="_" value={form.endereco.cep} onChange={e => changeFieldEndereco(e, e.target)} name="cep" mask="99999-999" />
                        </Form.Field>
                        <Form.Field width={5} required={provider.name==='HUBSALE'}>
                            <label>Logradouro</label>
                            <Form.Input name="logradouro" required={provider.name==='HUBSALE'} value={form.endereco.logradouro} onChange={changeFieldEndereco} />
                        </Form.Field>
                        <Form.Field width={2} required={provider.name==='HUBSALE'}>
                            <label>N°.</label>
                            <Form.Input name="numero" required={provider.name==='HUBSALE'} value={form.endereco.numero} onChange={changeFieldEndereco} />
                        </Form.Field>
                        <Form.Field width={3} required={provider.name==='HUBSALE'}>
                            <label>Bairro</label>
                            <Form.Input name="bairro" required={provider.name==='HUBSALE'} value={form.endereco.bairro} onChange={changeFieldEndereco} />
                        </Form.Field>
                        <Form.Field width={3} required={provider.name==='HUBSALE'}>
                            <label>Cidade</label>
                            <Form.Input name="cidade" required={provider.name==='HUBSALE'} value={form.endereco.cidade} onChange={changeFieldEndereco} />
                        </Form.Field>
                        <Form.Field width={1} required={provider.name==='HUBSALE'}>
                            <label>UF</label>
                            <Form.Input name="estado" required={provider.name==='HUBSALE'} value={form.endereco.estado} onChange={changeFieldEndereco} />
                        </Form.Field>
                    </Form.Group>

                    <Header as="h4" content="Informações de cartão" icon="credit card outline" />
                    <Form.Group>
                        <Form.Field width={8} required>
                            <label>Cartão</label>
                            <Form.Input name="card" required type="tel" value={form.card} onFocus={handleFocus('number')} onChange={changeField} />
                        </Form.Field>
                        <Form.Field width={3} required={provider.requires_cvv}>
                            <label>CVV</label>
                            <Form.Input name="cvv" required={provider.requires_cvv} value={form.cvv} onFocus={handleFocus('cvc')} onChange={changeField} />
                        </Form.Field>
                        <Form.Field width={5} required>
                            <label>Vencimento</label>
                            <InputMask mask="99/9999" required maskChar="_" value={form.expires} onChange={e => changeField(e, e.target)} name="expires" onFocus={handleFocus('expiry')} />
                        </Form.Field>
                    </Form.Group>
                    <Form.Field>
                        <label>Valor</label>
                        <NumberFormat disabled required value={form.amount} decimalSeparator={','} thousandSeparator={'.'} prefix="R$ " fixedDecimalScale={true} decimalScale={2} onChange={(e) => changeField(e, {name: 'amount', value: e.target.valueAsNumber})} />
                    </Form.Field>
                    <Message error icon="times circle" content={status.mensagem} />
                    <ErrorHandler provider={provider.name}>{erro}</ErrorHandler>
                    <Message success icon="check circle" content={status.mensagem} />
                    <Form.Button color={MAIN_COLOR}  loading={status.loading} disabled={status.loading} fluid content="Enviar" icon="credit card" />

                </Form>
            </Grid.Column>
            <Grid.Column width={5}>
            <Cards
                preview
                cvc={form.cvv}
                focus={foco}
                name={form.name}
                number={form.card}
                expiry={form.expires}
                placeholders={{
                    name: "Nome do titular"
                }}
                locale={{
                    valid: "Válido até"
                }}
                />
            </Grid.Column>
        </Grid>
    )
}

export default FormNovaVenda