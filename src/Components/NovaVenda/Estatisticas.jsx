import React, { useState, useEffect } from 'react'
import { Grid, Segment, Header, Statistic, Placeholder } from 'semantic-ui-react'
import { MAIN_COLOR } from '../../environment'
import Api from '../../Api'

const EstatisticaBlock = props => {
    const { loading } = props;

    if(loading){
        return <Segment textAlign="center" attached stacked padded color={props.color}>
            <Placeholder>
                <Placeholder.Header></Placeholder.Header>
                <Placeholder.Line></Placeholder.Line>
            </Placeholder>
        </Segment>
    }
    
    return (
        <Segment textAlign="center" attached stacked padded color={props.color}>
            <Statistic value={props.value || 0} label={props.title} />
        </Segment>
    )
}

const Estatisticas = props => {
    const { refresh } = props;
    const [loading, setLoading] = useState(false)
    const [estatisticas, setEstatisticas] = useState({});
    const [estatisticasDia, setEstatisticasDia] = useState({});

    const load = async (cancel) => {
        setLoading(true)
        try{
            const estatisticas = await Api.Sell.estatisticas(cancel)
            setEstatisticas(estatisticas)
            const estatisticasDia = await Api.Sell.estatisticas_dia(cancel)
            setEstatisticasDia(estatisticasDia)
        } catch(erro) {

        }
        setLoading(false)
    }

    useEffect(() => {
        const cancel = Api.Sell.getCancelToken();
        load(cancel)
        return () => cancel.cancel()
    }, [refresh])

    return <Grid columns="equal">
        <Grid.Column>
            <EstatisticaBlock color="green" title="Aprovadas Hoje" value={estatisticasDia.aprovadas} loading={loading} />
        </Grid.Column>
        <Grid.Column>
            <EstatisticaBlock color="green" title="Canceladas Hoje" value={estatisticasDia.canceladas} loading={loading} />
        </Grid.Column>
        <Grid.Column>
            <EstatisticaBlock color="blue" title="Aprovadas mês" value={estatisticas.aprovadas} loading={loading} />
        </Grid.Column>
        <Grid.Column>
            <EstatisticaBlock color="red" title="Canceladas mês" value={estatisticas.canceladas} loading={loading} />
        </Grid.Column>
    </Grid>
}

export default Estatisticas