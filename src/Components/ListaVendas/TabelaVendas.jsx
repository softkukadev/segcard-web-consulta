import { CPF } from "cpf_cnpj";
import React from "react";
import Moment from "react-moment";
import { useHistory } from "react-router-dom";
import { Dropdown, Menu, Pagination, Table } from "semantic-ui-react";
import { isFunction } from "util";
import Api from "../../Api";
import routes from "../../Navigator/routes";

const TabelaVendas = (props) => {
  const { vendas, callback, setPagina, currentRole } = props;

  const history = useHistory();

  const unsubscribe = async (provider, id) => {
    try {
      const unsub = await Api.Sell.cancel_sale(provider, id);
      isFunction(callback) && callback();
    } catch (erro) {}
  };

  return (
    <>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={6}>Nome</Table.HeaderCell>
            <Table.HeaderCell>CPF</Table.HeaderCell>
            <Table.HeaderCell>Telefone</Table.HeaderCell>
            <Table.HeaderCell>Cartão</Table.HeaderCell>
            {currentRole.role === "administrador" && (
              <Table.HeaderCell>Provedor</Table.HeaderCell>
            )}
            <Table.HeaderCell>Status</Table.HeaderCell>
            {currentRole.role !== "operador" && (
              <Table.HeaderCell>Vendedor</Table.HeaderCell>
            )}
            <Table.HeaderCell>Filial</Table.HeaderCell>
            <Table.HeaderCell textAlign="right">Data</Table.HeaderCell>
            <Table.HeaderCell width={1} textAlign="right">
              Ações
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {vendas.docs &&
            vendas.docs.map((venda) => (
              <Table.Row key={venda._id}>
                <Table.Cell>{venda.name}</Table.Cell>
                <Table.Cell>{CPF.format(venda.cpf)}</Table.Cell>
                <Table.Cell>{venda.phone_number}</Table.Cell>
                <Table.Cell>{venda.card}</Table.Cell>
                {currentRole.role === 'administrador' && (
                  <Table.Cell>{venda.provider}</Table.Cell>
                )}
                <Table.Cell>{venda.status}</Table.Cell>
                {currentRole.role !== "operador" && (
                  <Table.Cell>{venda.operador.name}</Table.Cell>
                )}
                <Table.Cell>
                <strong>NOME DO FILIAL</strong>
                {/* {venda.nomeFilial} */}
                </Table.Cell>
                <Table.Cell textAlign="right">
                  <Moment format="DD/MM/YYYY HH:mm:ss">
                    {venda.created_at}
                  </Moment>
                </Table.Cell>
                <Table.Cell width={1} textAlign="right">
                  <Dropdown icon="bars">
                    <Dropdown.Menu>
                      {venda.status !== "suspended" && (
                        <Menu.Item
                          icon="times circle"
                          onClick={() => unsubscribe(venda.provider, venda._id)}
                          content="Cancelar"
                        />
                      )}
                      {["sk-payu", 'pagseguro', 'vindi', 'rede'].includes(venda.provider) && (
                        <Menu.Item
                          icon="search"
                          onClick={() =>
                            history.push(
                              routes.VISUALIZAR_ASSINATURA.replace(
                                ":id",
                                venda._id
                              )
                            )
                          }
                          content="Visualizar assinatura"
                        />
                      )}
                    </Dropdown.Menu>
                  </Dropdown>
                </Table.Cell>
              </Table.Row>
            ))}
        </Table.Body>
      </Table>
      <Pagination
        totalPages={(vendas && vendas.totalPages) || 1}
        defaultActivePage={1}
        onPageChange={(e, t) => setPagina(t.activePage)}
      />
    </>
  );
};

export default TabelaVendas;
