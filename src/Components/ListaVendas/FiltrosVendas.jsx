import React, { useState, useEffect } from 'react'
import InputMask from 'react-input-mask'
import { Form, Header, Button, Select } from 'semantic-ui-react';
import { MAIN_COLOR } from './../../environment'
import Payment from 'payment'
import SelectOperador from '../Selects/SelectOperador';
import Api from '../../Api';

const ESTADO_INICIAL_FILTROS = {
    name: '',
    operador: '',
    cpf: '',
    status: '',
    gateway: '',
    cartao: '',
    filial: '',
    recorrencia: '',
}

const optionsGateway = [
    {key: 1, text: 'Cielo', value: 'cielo'},
    {key: 2, text: 'Fivepay', value: 'fivepay'},
]
const optionsRec = [{
    text: 'Todos',
    key: 'todos', 
    value: ''
},
{
    text: 'Recorrências',
    key: 'sim', 
    value: true
}, {
    text: 'Primeiro Pagamento',
    key: 'não', 
    value: false
}]

const optionsStatus = [{
    text: 'Todos',
    key: 'todos', 
    value: ''
},
{
    text: 'Criado',
    key: 'created', 
    value: 'created'
}, {
    text: 'Falha',
    key: 'FAILED', 
    value: 'FAILED' 
}]

const FiltrosVendas = props => {
    const { applyFilters, currentRole, tipo } = props
    const [filtros, setFiltros] = useState({...ESTADO_INICIAL_FILTROS})
    const [open, setOpen] = useState(false)
    const [filiais, setFiliais] = useState([]);

    useEffect(() => {
        async function getFiliais(cancel) {
          try {
            const resp = await Api.Filial.list_all(cancel);
            setFiliais(resp);
          } catch(error) {
            setFiliais([])
          }
        }
        const cancelToken = Api.Filial.getCancelToken()
        getFiliais(cancelToken)
        return () => {
          cancelToken.cancel()
        }
      }, [])

    const changeFilters = (e, { name, value }) => {
        setFiltros({...filtros, [name]: value})
    }

    useEffect(() => {
        if(open){
            Payment.formatCardNumber(document.getElementsByName("cartao")[0]);
        }
    }, [ open ])

    const optionsFilial = React.useMemo(() => {
        return filiais.map(filial => ({ key: filial._id, value: filial._id, text: filial.nomeFilial.toUpperCase() }))
      }, [filiais])

    if(!open) {
        return (
            <Header as="h3">
                <Button content="Exibir filtros" icon="filter" floated="right" compact onClick={() => setOpen(true)} color={MAIN_COLOR} basic />
                <Header.Content>
                    Filtros
                </Header.Content>
            </Header>
        )
    }

    

    return (
        <>
            <Header as="h3">
                <Button content="Ocultar filtros" icon="filter" floated="right" compact onClick={() => setOpen(false)} color={MAIN_COLOR} basic />
                <Header.Content>
                    Filtros
                </Header.Content>
            </Header>
            <Form>
                <Form.Group>
                    <Form.Field width={6}>
                        <label>CPF</label>
                        <InputMask mask="999.999.999-99" required maskChar="_" value={filtros.cpf} onChange={e => changeFilters(e, e.target)} name="cpf" />
                    </Form.Field>
                    <Form.Field width={10}>
                        <label>Nome</label>
                        <Form.Input name="name" onChange={changeFilters} value={filtros.name} />
                    </Form.Field>
                </Form.Group>

                <Form.Group>
                    <Form.Field width={4}>
                        <label>Cartão</label>
                        <Form.Input name="cartao" onChange={changeFilters} value={filtros.cartao} />
                    </Form.Field>

                    <Form.Field width={4}>
                        <label>Status</label>
                        <Form.Select name="status" onChange={changeFilters} value={filtros.status} options={optionsStatus} />
                    </Form.Field>

                { (currentRole.role === 'administrador' || currentRole.role === 'backoffice') &&
                        <Form.Field width={4}>
                            <label>Operador</label>
                            <SelectOperador name="operador" onChange={changeFilters} value={filtros.operador} />
                        </Form.Field>
                }
                { (currentRole.role === 'administrador') &&
                        <Form.Field width={4}>
                            <label>Filial</label>
                            <Select name="filial" options={optionsFilial} onChange={changeFilters} value={filtros.filial} />
                        </Form.Field>
                }
                </Form.Group>

                { tipo === 'pagamentos' && <Form.Group>
                    <Form.Field width={4}>
                        <label>Tipo pagamento</label>
                        <Select name="recorrencia" options={optionsRec} onChange={changeFilters} value={filtros.recorrencia} />
                    </Form.Field>

                </Form.Group>}

                <Form.Button content="Aplicar filtros" onClick={() => applyFilters(filtros)} fluid color={MAIN_COLOR} />
            </Form>
        </>
    )
}

export default FiltrosVendas