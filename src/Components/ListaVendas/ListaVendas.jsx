import React, { useState, useEffect, useContext } from 'react'
import Api from '../../Api'
import TabelaVendas from './TabelaVendas'
import InputMask from 'react-input-mask'
import { Segment, Form, Header, Divider, Button } from 'semantic-ui-react';
import FiltrosVendas from './FiltrosVendas';
import FilialContext from '../../Helpers/FilialContext';

const ESTADO_INICIAL_FILTROS = {
    name: '',
    operador: '',
    cpf: '',
    status: '',
    gateway: '',
    cartao: ''
}

const ListaVendas = props => {
    const { currentRole, refresh, callback, ...segmentProps } = props;
    const [vendas, setVendas] = useState({});
    const [paginaAtual, setPaginaAtual] = useState(1);
    const [loading, setLoading] = useState(false);
    const [filtros, setFiltros] = useState({...ESTADO_INICIAL_FILTROS})

    const filialContext = useContext(FilialContext)

    async function load(cancelToken, pagina, filtros) {
        setLoading(true);
        try{
            const itens = await Api.Sell.list_sells(cancelToken, pagina, filtros)
            setVendas(itens)
        } catch(error) {

        }finally{
            setLoading(false);
        }
    }

    const exportar = async () => {
        const cancelToken = Api.Sell.getCancelToken()
        const itens = await Api.Sell.list_sells(cancelToken, undefined, filtros, 'csv')

        window.URL = window.webkitURL || window.URL;
        var csvFile = new Blob([itens], {
            type: 'text/csv; charset=utf-8'
        });
        var link = document.createElement("a");
        link.setAttribute("href", window.URL.createObjectURL(csvFile));
        link.setAttribute("download", "assinaturas.csv"); 
        link.dataset.downloadurl = ['text/csv', link.download, link.href].join(':');
        document.body.appendChild(link);
        link.click();
        link.remove()
    }

    useEffect(() => {
        const cancelToken = Api.Sell.getCancelToken()
        load(cancelToken, paginaAtual, filtros)
        return () => cancelToken.cancel()
    }, [paginaAtual, filtros, refresh])

    return (
        <Segment loading={loading} {...segmentProps}>
            <FiltrosVendas applyFilters={(filters) => setFiltros(filters)} currentRole={currentRole} />
            <TabelaVendas currentRole={currentRole} callback={callback} vendas={vendas} setPagina={setPaginaAtual} />
            { !filialContext.isFilial && <Button floated="right" content="Exportar para CSV" onClick={exportar} color="green" icon="table" /> }
            <div style={{clear: 'both'}} />
        </Segment>
    )
}

export default ListaVendas