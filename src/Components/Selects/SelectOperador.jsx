import React, { useState, useEffect } from 'react'
import { Form } from 'semantic-ui-react';
import Api from '../../Api';


const SelectOperador = props => {
    const [operadores, setOperadores] = useState([]);
    const [operadoresOptions, setOperadoresOptions] = useState([])
    const [filter, setFilter] = useState('')
    const [loading, setLoading] = useState(false);

    async function load(cancel, filter) {
        setLoading(true);
        try{
            const ops = await Api.Auth.list_all(cancel, 1, filter)
            setOperadores(ops.docs)
        } catch(erro) {
            setOperadores([])
        }finally{
            setLoading(false);
        }
    }
    
    useEffect(() => {
        const cancelToken = Api.Auth.getCancelToken()
        load(cancelToken, filter);
        return () => cancelToken.cancel()
    }, [filter])

    useEffect(() => {
        const opts = operadores.map(op => ({
            key: op._id,
            text: op.name,
            value: op._id
        }))
        setOperadoresOptions(opts)
    }, [operadores])

    const onQueryChange = (e, { searchQuery }) => {
        setFilter(searchQuery)
    }

    return <Form.Select search clearable options={operadoresOptions} {...props} onSearchChange={onQueryChange} noResultsMessage="Nenhum resultado" loading={loading} />
}



export default SelectOperador