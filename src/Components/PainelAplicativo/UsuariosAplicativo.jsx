import React, { useCallback, useEffect, useState } from 'react'
import { Button, Header, Label, Pagination, Table } from 'semantic-ui-react';
import { MAIN_COLOR } from '../../environment';
import ConfirmacaoResetSenha from './ConfirmacaoResetSenha';
import moment from 'moment'
import Moment from 'react-moment';
import Api from '../../Api';

const MOCK_DATA = [
    { nome: 'Daniel Simas', cpf: '000.000.000-22', email: 'daniel@segcard.com.br', status: 'ativo', ultimoAcesso: moment().add(-10, 'day').toDate() },
    { nome: 'Ronaldo Bicca', cpf: '000.000.000-32', email: 'ronaldo@segcard.com.br', status: 'ativo', ultimoAcesso: moment().add(-2, 'day').toDate() },
    { nome: 'Samuel de oliveira', cpf: '000.000.000-42', email: 'samuel@segcard.com.br', status: 'ativo', ultimoAcesso: moment().add(-1, 'day').toDate() },
    { nome: 'Kalil K. V. Junior', cpf: '000.000.000-52', email: 'kkv@segcard.com.br', status: 'ativo', ultimoAcesso: moment().add(-0, 'day').toDate() },
]

const UsuariosAplicativo = props => {

    const [paginacao, setPaginacao] = useState({ page: 1, totalPages: 1 })
    const [usuarios, setUsuarios] = useState([]);
    const [loading, setLoading] = useState(false)

    const [usuarioRecuperacao, setUsuarioRecuperacao] = useState(null)
    const isOpenConfirmacao = React.useMemo(() => usuarioRecuperacao !== null, [usuarioRecuperacao])

    const load = useCallback(async page => {
        try {            
            const { docs, totalPages } = await Api.UsuariosAplicativo.getUsuarios(page);
            setUsuarios(docs);

            setPaginacao(paginacao => ({ ...paginacao, totalPages, page }))
        } catch(err) {
            setUsuarios([])
        } finally{
            setLoading(false)
        }
    }, [])

    useEffect(() => {
        load(1)
    }, [load])


    return <>
        <Header as="h4">
            <Header.Content>Lista de usuários</Header.Content>
        </Header>
        <Table celled>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell width={4}>Nome</Table.HeaderCell>
                    <Table.HeaderCell width={2}>CPF</Table.HeaderCell>
                    <Table.HeaderCell width={2}>Status</Table.HeaderCell>
                    <Table.HeaderCell>Email</Table.HeaderCell>
                    <Table.HeaderCell width={2}>Último acesso</Table.HeaderCell>
                    <Table.HeaderCell width={2}>Ações</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {usuarios.map(usuario => <Table.Row key={usuario.nome}>
                <Table.Cell>{usuario.name}</Table.Cell>
                    <Table.Cell>{usuario.cpf}</Table.Cell>
                    <Table.Cell>{usuario.enabled ? 'Ativo' : 'Desativado'}</Table.Cell>
                    <Table.Cell>
                        {usuario.email}
                    </Table.Cell>
                    <Table.Cell><Moment format="DD/MM/YYYY HH:mm">{usuario.ultimoAcesso}</Moment></Table.Cell>
                    <Table.Cell>
                        <Button icon="key" onClick={() => setUsuarioRecuperacao(usuario)} title="Recuperação de senha" color={MAIN_COLOR}></Button>
                        <Button icon="remove user" title="Desativar Acesso" color={MAIN_COLOR}></Button>
                    </Table.Cell>
                </Table.Row>)}
            </Table.Body>
        </Table>
        <Pagination totalPages={paginacao.totalPages} onPageChange={(e,p) => load(p.activePage)} defaultActivePage={1} />
        <ConfirmacaoResetSenha open={isOpenConfirmacao} usuario={usuarioRecuperacao} onClose={() => setUsuarioRecuperacao(null)} />
    </>
}

export default UsuariosAplicativo;