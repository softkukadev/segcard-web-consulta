import React, { useState, useCallback, useEffect } from 'react'
import { useRef } from 'react'
import { Button, Form, Header, Icon, Modal } from 'semantic-ui-react'
import { MAIN_COLOR } from '../../environment'
import ShutterSelector from '../../Helpers/ShutterSelector/ShutterSelector'

const ConfirmacaoResetSenha = ({ open, usuario, onClose }) => {
    if (!open) {
        return <></>
    }

    return <Modal open={open} onClose={onClose} closeIcon>
        <Modal.Header>
            <Header content="Confirmação de envio de recuperação de senha"></Header>
        </Modal.Header>
        <Modal.Content>

        </Modal.Content>
        <Modal.Actions >
            <Button basic color={MAIN_COLOR} onClick={() => onClose(false)}>
                <Icon name='times circle' /> Cancelar 
            </Button>
            <Button color={MAIN_COLOR} onClick={() => null}>
                <Icon name='check circle' /> Confirmar
            </Button>
        </Modal.Actions>
    </Modal>
}

export default ConfirmacaoResetSenha