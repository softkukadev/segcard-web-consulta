import React, { useState, useCallback, useEffect } from 'react'
import { useRef } from 'react'
import { Button, Form, Header, Icon, Modal, Pagination } from 'semantic-ui-react'
import Api from '../../Api'
import { MAIN_COLOR } from '../../environment'
import ShutterSelector from '../../Helpers/ShutterSelector/ShutterSelector'

const data = [
    'aaaaaaaaaaaaa',
    'abaaaaaaaaaaa',
    'acaaaaaaaaaaa',
    'adaaaaaaaaaaa',
    'aeaaaaaaaaaaa',
    'afaaaaaaaaaaa',
    'agaaaaaaaaaaa',
    'ahaaaaaaaaaaa',
    'aiaaaaaaaaaaa',
    'ajaaaaaaaaaaa',
    'akaaaaaaaaaaa',
    'alaaaaaaaaaaa',
    'amaaaaaaaaaaa',
    'anaaaaaaaaaaa',
]

const NovaNotificacao = ({ open, onClose }) => {
    const shutter = useRef(null);

    const [paginacao, setPaginacao] = useState({ page: 1, totalPages: 1 })
    const [usuarios, setUsuarios] = useState([]);
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState({
        titulo: '',
        mensagem: ''
    })
    
    const load = useCallback(async page => {
        try {            
            const { docs, totalPages } = await Api.UsuariosAplicativo.getUsuarios(page);
            setUsuarios(docs);

            setPaginacao(paginacao => ({ ...paginacao, totalPages, page }))
        } catch(err) {
            setUsuarios([])
        } finally{
            setLoading(false)
        }
    }, [])

    useEffect(() => {
        load(1)
    }, [load, open])

    useEffect(() => {
        if (shutter.current && open) {
            shutter.current.setRegistros(usuarios);
        }
    }, [shutter, open, usuarios])


    const submit = useCallback(() => {
        const formValue = { ...data, destinos: shutter.current.getSelecionados()}
        console.log(formValue);
    }, [shutter, data])

    const onChange = useCallback((e, { name, value }) => {
        setData(data => ({ ...data, [name]: value }))
    }, [])

    if (!open) {
        return <></>
    }

    return <Modal open={open} onClose={onClose} closeIcon>
        <Modal.Header>
            <Header content="Nova notificação"></Header>
        </Modal.Header>
        <Modal.Content>
            <Form>
                <Form.Field required>
                    <label>Título</label>
                    <Form.Input name="titulo" onChange={onChange} value={data.titulo} required ></Form.Input>
                </Form.Field>
                <Form.Field required>
                    <label>Destinatários</label>
                    <ShutterSelector ref={shutter} renderItem={(item) => item.name} keyExtractor={item => item._id} />
                    <div style={{gridTemplateColumns: '1fr 50px 1fr', marginTop: 5}}>
                        <Pagination totalPages={paginacao.totalPages} onPageChange={(e,p) => load(p.activePage)} defaultActivePage={1} />
                    </div>
                </Form.Field>
                <Form.Field required>
                    <label>Mensagem</label>
                    <Form.TextArea name="mensagem" onChange={onChange} value={data.mensagem} required ></Form.TextArea>
                </Form.Field>
            </Form>
        </Modal.Content>
        <Modal.Actions >
            <Button basic color={MAIN_COLOR} onClick={() => onClose(false)}>
                <Icon name='times circle' /> Cancelar 
            </Button>
            <Button color={MAIN_COLOR} onClick={submit}>
                <Icon name='check circle' /> Enviar notificação
            </Button>
        </Modal.Actions>
    </Modal>
}

export default NovaNotificacao