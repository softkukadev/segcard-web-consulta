import React, { useState, useCallback, useEffect } from 'react'
import moment from 'moment'
import { Grid, Header, Label, Menu } from 'semantic-ui-react'
import styled from 'styled-components'
import { Route, Router, Switch, useHistory, useLocation, useRouteMatch } from 'react-router-dom'
import { createBrowserHistory } from "history";
import UsuariosAplicativo from './UsuariosAplicativo'
import NotificacoesAplicativo from './NotificacoesAplicativo'
import { MAIN_COLOR } from '../../environment'

const Wrapper = styled.div`
    padding: 1em 0;
    height: calc(97vh - 100px);
`


const PainelAplicativo = props => {

    let { path, url } = useRouteMatch();
    let history = useHistory()
    let route = useRouteMatch()
    const location = useLocation();

    const navigate = useCallback((to) =>  {
        history.push(to);
    }, [history])

    return <Wrapper>
        <Header as="h1" dividing>Painel de controle aplicativo Segcard</Header>
        <Grid style={{height: '100%'}}>
            <Grid.Column stretched width={3}>
                <Menu size='large' tabular vertical fluid>
                    <Menu.Item content="Menu" icon="bars" />
                    <Menu.Item content="Usuários" active={location.pathname.endsWith('usuarios')} icon="users" onClick={() => navigate(`${url}/usuarios`)} />
                    <Menu.Item content="Notificações" active={location.pathname.endsWith('notificacoes')} icon="talk"onClick={() => navigate(`${url}/notificacoes`)} />
                </Menu>
            </Grid.Column>
            <Grid.Column  width={13}>
            <Switch>
                <Route exact path={`${path}`}>
                    <Label pointing="left" size="huge" color={MAIN_COLOR}>Selecione um dos menus ao lado para realizar as ações</Label>
                </Route>
                <Route exact path={`${path}/usuarios`}>
                    <Header as="h2" dividing>Usuários do aplicativo</Header>
                    <UsuariosAplicativo />
                </Route>
                <Route path={`${path}/notificacoes`}>
                    <Header as="h2" dividing>Notificações</Header>
                    <NotificacoesAplicativo />
                </Route>
            </Switch>
            </Grid.Column>
        </Grid>
    </Wrapper>
}

export default PainelAplicativo