import React, { useState } from 'react'
import { Button, Header, Label, Table } from 'semantic-ui-react'
import { MAIN_COLOR } from '../../environment';
import NovaNotificacao from './NovaNotificacao';

const NotificacoesAplicativo = props => {
    const [novaNotificacao, setNovaNotificacao] = useState(false);

    return <>
        <Header as="h4">
            <Header.Content>Histórico de notificações</Header.Content>
            <Button onClick={() => setNovaNotificacao(true)} floated="right" compact circular content="Nova notificação" icon="add circle"></Button>
        </Header>

        <br/>
        <Table celled>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell width={3}>Data envio</Table.HeaderCell>
                    <Table.HeaderCell>Conteúdo</Table.HeaderCell>
                    <Table.HeaderCell width={2}>Número de destinatários</Table.HeaderCell>
                    <Table.HeaderCell width={2}>Ações</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                <Table.Row>
                    <Table.Cell>10/11/2020 11:11</Table.Cell>
                    <Table.Cell>Notificação de teste</Table.Cell>
                    <Table.Cell>
                        <Label color={MAIN_COLOR} content={6} />
                    </Table.Cell>
                    <Table.Cell></Table.Cell>
                </Table.Row>
            </Table.Body>
        </Table>
        <NovaNotificacao open={novaNotificacao} onClose={() => setNovaNotificacao(false)} />
    </>
}

export default NotificacoesAplicativo;