import React from "react";
import { Message } from "semantic-ui-react";
import { isArray } from "util";

const ErrorHandler = ({ children, provider }) => {

  if(!children) {
    return <></>
  }

  if (provider === "PAYLAB") {
    return (
      <Message error>
        <ul>
          {isArray(children.Messages) &&
            children.Messages.map((message) => (
              <li key={message.code}>
                <em>{message.Code}</em>: {message.Message}
              </li>
            ))}
        </ul>
      </Message>
    );
  }
  if (provider === "PAGSEGURO") {
    return (
      <Message error>
        <ul>
          <li>
            <em>Código {children?.payment_response?.code}</em>: {children?.payment_response?.message}
          </li>
        </ul>
      </Message>
    );
  }
  if (provider === "CIELO") {
    return (
      <Message error>
        <ul>
          <li>
            <em>Código {children.ReasonCode}</em>: {children.ReasonMessage} (
            {children.message})
          </li>
        </ul>
      </Message>
    );
  }
  if (provider === "CIELO") {
    return (
      <Message error>
        <ul>
          <li>
            <em>Código {children.ReasonCode}</em>: {children.ReasonMessage} (
            {children.message})
          </li>
        </ul>
      </Message>
    );
  }
  if (provider === "HUBSALE") {
    return (
      <Message error>
        <ul>
          <li>
            <em>Código {children.status_code || ''}</em>: {children.status_text || (Array.isArray(children?.error) && children?.error.length > 0 && children?.error[0].message)} (
              {Array.isArray(children?.status_log) && children.status_log[children.status_log.length - 1].message}
            )
          </li>
        </ul>
      </Message>
    );
  }
  if (provider === "SK-PAYU") {
    return (
      <Message error>
        <ul>
          <li>
            <em>
              Código {children.paymentNetworkResponseCode ?? children?.error ?? children?.erro} -{" "}
              {children.responseCode}
            </em>
            : {children.paymentNetworkResponseErrorMessage}
          </li>
        </ul>
      </Message>
    );
  }
  if (provider === "VINDI") {
    return (
      <Message error>
        <ul>
          <li>
            <em>
              {children.charges?.reduce((acc, item) => {
                return acc + `Codigo: ${item?.last_transaction?.gateway_response_code}: ${item?.last_transaction?.gateway_message}  `
              }, '')}
            </em>
          </li>
        </ul>
      </Message>
    );
  }
  if (provider === "REDE") {
    return (
      <Message error>
        <ul>
          <li>
            <em>
              Código {children.paymentNetworkResponseCode ?? children?.error ?? children?.erro} -{" "}
              {children.responseCode}
            </em>
            : {children.paymentNetworkResponseErrorMessage}
          </li>
        </ul>
      </Message>
    );
  }
  return <Message error></Message>;
};

export default ErrorHandler;
