import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Segment, Header, Label, Button, Divider, Table, Icon, Loader, Dimmer } from 'semantic-ui-react'
import Api from '../../Api'
import { MAIN_COLOR } from '../../environment'
import EdicaoProvedores from './EdicaoProvedores'

const ListaSeguradoras = props => {
    const [loading, setLoading] = useState(false);
    const [seguradoras, setSeguradoras] = useState([]);

    const [seguradoraEdicao, setSeguradoraEdicao] = useState(null);

    async function load(cancelToken){
        setLoading(true);
        try{
            const provs = await Api.Seguradora.list_seguradoras(cancelToken)
            setSeguradoras(provs)
        } catch(error) {

        } finally{
            setLoading(false)
        }
    }

    useEffect(() => {
        const cancelToken = Api.Seguradora.getCancelToken()
        load(cancelToken)
        return () => cancelToken.cancel()
    }, [])

    const changeDefaultProvider = (id) => async () => {
        try{
            await Api.Seguradora.set_default(id);
            const cancel = Api.Seguradora.getCancelToken()
            load(cancel)
        } catch(erro){

        }
    }

    const closeEdicao = () => {
        setSeguradoraEdicao(null);
        const cancel = Api.Seguradora.getCancelToken()
        load(cancel)
    }

    return (
        <>
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Nome</Table.HeaderCell>
                        <Table.HeaderCell width={4} textAlign="right">Ações</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    { seguradoras.map(seguradora => (
                        <Table.Row key={seguradora._id}>
                            <Table.Cell>{seguradora.name}</Table.Cell>
                            <Table.Cell></Table.Cell>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
            <EdicaoProvedores provider={seguradoraEdicao} close={closeEdicao} />
            <Dimmer inverted active={loading}>
                <Loader>Carregando...</Loader>
            </Dimmer>
        </>
    )
}

const mapStateToProps = state => ({
    provider: state.provider
});
  
export default connect(mapStateToProps)(ListaSeguradoras)