import React from 'react'
import { Segment, Header } from 'semantic-ui-react'
import ListaProviders from './ListaProviders'
import Parametros from '../Parametros/Parametros'
import ListaSeguradoras from './ListaSeguradoras'
import EscalonadorVendas from '../../Helpers/EscalonadorVendas'

const Configuracoes = props => {
    return (
        <>
            <Header as="h2" attached="top">
                <Header.Content>
                    Provedores
                </Header.Content>
            </Header>
            <Segment attached padded>
                <ListaProviders />
            </Segment>
            <Header as="h2" attached="top">
                <Header.Content>
                    Seguradoras
                </Header.Content>
            </Header>
            <Segment attached padded>
                <ListaSeguradoras />
            </Segment>
            <Header as="h2" attached="top">
                <Header.Content>
                    Parâmetros
                </Header.Content>
            </Header>
            <Segment attached padded>
                <Parametros />
            </Segment>
            <Segment>
                <EscalonadorVendas />
            </Segment>
            <br/>
            <br/>
        </>
    )
}
  
export default Configuracoes