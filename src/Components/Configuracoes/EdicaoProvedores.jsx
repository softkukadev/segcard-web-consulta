import React, { useState, useEffect } from 'react'
import { Modal, Header, Form, Divider, Message } from 'semantic-ui-react';
import { MAIN_COLOR } from '../../environment';
import Api from '../../Api';

const EdicaoProvedores = props => {
    const { provider, close } = props;
    const [form, setForm] = useState(null)
    const [erro, setErro] = useState(false);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if(provider){
            setForm({...provider})
        } else{
            setForm(null)
        }
    }, [provider])
    
    if(!provider || !form) {
        return <></>
    }

    const onChange = (e, { name, value }) => {
        setForm({...form, [name]: value})
    }

    const onChangeChecked = (e, { name, checked }) => {
        setForm({...form, [name]: checked})
    }
    
    const changeProvider = (e, { name, value }) => {
        setForm({...form, credentials: {...form.credentials, [name]: value}})
    }

    async function submit() {
        setErro(null);
        setLoading(true);
        try{
            const saved = await Api.Provider.update_provider(form._id, form)
            setLoading(false);
            if(saved) {
                close()
            }
        } catch(error) {
            setErro(true);
            setLoading(false);
        }
    }


    return (
        <Modal open={provider !== null} closeIcon onClose={close} closeOnDimmerClick={false} >
            <Modal.Header>
                <Header as="h2" content={`Editando Integrador ${provider.name}`} />
            </Modal.Header>
            <Modal.Content>
                <Form loading={loading} error={erro} onSubmit={submit}>
                    <Form.Field required>
                        <label>Nome</label>
                        <Form.Input required value={form.name} name="name" onChange={onChange} />
                    </Form.Field>
                    <Form.Field required>
                        <label>Precisa de cvv?</label>
                        <Form.Checkbox toggle checked={form.requires_cvv} name="requires_cvv" onChange={onChangeChecked} />
                    </Form.Field>
                    <Divider />
                    <Header as="h5" content="Credenciais" />
                    {
                        Object.keys(form.credentials).map(key => (
                            <Form.Field key={key}>
                                <label>{key}</label>
                                <Form.Input value={form.credentials[key]} name={key} onChange={changeProvider} />
                            </Form.Field>
                        ))
                    }
                    <Message error content="Erro ao salvar os dados do Integrador" />
                    <Form.Button icon="save" content="Salvar" fluid color={MAIN_COLOR} />
                </Form>
            </Modal.Content>
        </Modal>
    )

}

export default EdicaoProvedores