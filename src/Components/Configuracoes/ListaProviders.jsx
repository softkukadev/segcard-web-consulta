import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Segment, Header, Label, Button, Divider, Table, Icon, Loader, Dimmer } from 'semantic-ui-react'
import Api from '../../Api'
import { MAIN_COLOR } from '../../environment'
import EdicaoProvedores from './EdicaoProvedores'

const Configuracoes = props => {
    const { provider } = props
    const [loading, setLoading] = useState(false);
    const [providers, setProviders] = useState([]);

    const [providerEdicao, setProviderEdicao] = useState(null);

    async function load(cancelToken){
        setLoading(true);
        try{
            const provs = await Api.Provider.list_providers(cancelToken)
            setProviders(provs)
        } catch(error) {

        } finally{
            setLoading(false)
        }
    }

    useEffect(() => {
        const cancelToken = Api.Provider.getCancelToken()
        load(cancelToken)
        return () => cancelToken.cancel()
    }, [])

    const changeDefaultProvider = (id) => async () => {
        try{
            await Api.Provider.set_default(id);
            const cancel = Api.Provider.getCancelToken()
            load(cancel)
        } catch(erro){

        }
    }
    
    const changeDefaultProviderRecorrencia = (id) => async () => {
        try{
            await Api.Provider.set_default_recorrencia(id);
            const cancel = Api.Provider.getCancelToken()
            load(cancel)
        } catch(erro){

        }
    }

    const closeEdicao = () => {
        setProviderEdicao(null);
        const cancel = Api.Provider.getCancelToken()
        load(cancel)
    }

    return (
        <>
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Nome</Table.HeaderCell>
                        <Table.HeaderCell>Endpoint</Table.HeaderCell>
                        <Table.HeaderCell>Precisa CVV?</Table.HeaderCell>
                        <Table.HeaderCell width={4} textAlign="right">Padrão</Table.HeaderCell>
                        <Table.HeaderCell width={4} textAlign="right">Padrão recorrência</Table.HeaderCell>
                        <Table.HeaderCell width={1} textAlign="right">Editar</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    { providers.map(provider => (
                        <Table.Row key={provider._id}>
                            <Table.Cell>{provider.name}</Table.Cell>
                            <Table.Cell>{provider.endpoint}</Table.Cell>
                            <Table.Cell>
                                {provider.requires_cvv ? 
                                    <Icon color="green" name="check circle" /> : 
                                    <Icon color="red" name="times circle" />
                                }
                            </Table.Cell>
                            <Table.Cell textAlign="right">
                                {provider.default && <Label color="green" icon="check" content="Ativo como padrão" />}
                                {!provider.default && <Button compact onClick={changeDefaultProvider(provider._id)} icon="check circle outline" content="Definir como padrão" color={MAIN_COLOR} />}
                            </Table.Cell>
                            <Table.Cell textAlign="right">
                                {provider.default_recorrencia && <Label color="green" icon="check" content="Ativo como padrão" />}
                                {!provider.default_recorrencia && <Button compact onClick={changeDefaultProviderRecorrencia(provider._id)} icon="check circle outline" content="Definir como padrão rec." color={MAIN_COLOR} />}
                            </Table.Cell>
                            <Table.Cell textAlign="right">
                                <Button icon="pencil" color={MAIN_COLOR} basic title="Editar" onClick={() => setProviderEdicao(provider)} />
                            </Table.Cell>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
            <EdicaoProvedores provider={providerEdicao} close={closeEdicao} />
            <Dimmer inverted active={loading}>
                <Loader>Carregando...</Loader>
            </Dimmer>
        </>
    )
}

const mapStateToProps = state => ({
    provider: state.provider
});
  
export default connect(mapStateToProps)(Configuracoes)