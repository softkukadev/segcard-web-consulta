import React, { useState, useEffect } from 'react'
import { Segment, Header } from 'semantic-ui-react'
import ListaUsuarios from './ListaUsuarios'
import ListaVendas from '../ListaVendas/ListaVendas'
import FormUsuario from './FormUsuario'
import { useParams } from 'react-router-dom'
import Api from '../../Api'

const Usuarios = props => {
    const [refresh, setRefresh] = useState(1);
    const [filial, setFilial] = useState(null)

    const {id: idFilial} = useParams();

    useEffect(() => {
        async function findFilial(cancel, id) {
            try {
                const resp = await Api.Filial.list_all(cancel)
                if(Array.isArray(resp)){
                    setFilial(resp.find(f => f._id === id))
                }
            } catch(error){

            }
        }
        const cancel = Api.Filial.getCancelToken()
        if(idFilial) {
            findFilial(cancel, idFilial)
        }
        return () => cancel.cancel()
    }, [idFilial])

    

    return (
        <>
            <Header as="h2" attached="top">
                <Header.Content>
                    Cadastro de usuário { filial && ` - ${filial?.nomeFilial}`}
                </Header.Content>
            </Header>
            <Segment  attached padded>
                <FormUsuario refresh={() => setRefresh(refresh+1)} />
            </Segment>

            <Header as="h2" attached="top">
                <Header.Content>
                    Usuários
                </Header.Content>
            </Header>
            <Segment attached padded>
                <ListaUsuarios refresh={() => setRefresh(refresh+1)} shouldRefresh={refresh}/>
            </Segment>
        </>
    )
}
  
export default Usuarios