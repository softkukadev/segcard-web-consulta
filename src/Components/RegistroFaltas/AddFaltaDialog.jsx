import React from 'react'
import { useCallback } from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import { Divider, Form, Header, Modal } from 'semantic-ui-react'
import SelectOperador from '../Selects/SelectOperador'
import {
    DateInput,
  } from 'semantic-ui-calendar-react';

import moment from 'moment'

import Api from '../../Api'

const AddFaltaDialog = props => {
    const { open, onClose } = props
    const [falta, setFalta] = useState({
        operador_id: '',
        data: '',
        justificativa: ''
    });
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if(open) {
            setFalta({
                operador_id: '',
                data: '',
                justificativa: ''
            })
        }
    }, [open])

    const submit = useCallback(async (dados) => {
        try {
            setLoading(true);
            if(dados.data && dados.operador_id) {
                dados.data = moment(dados.data, 'DD/MM/YYYY').toDate()
                const falta = await Api.Payroll.registrarFalta(dados)
                onClose();
            }
        } catch(error) { 

        } finally{
            setLoading(false);
        }
    }, [onClose])

    const onChange = useCallback((e, { name, value }) => {
        setFalta(falta => ({ ...falta, [name]: value }))
    }, [])
    
    return <Modal open={open} onClose={onClose} closeIcon>
        <Modal.Content>
            <Modal.Header>
                <Header as="h1" content="Cadastro de falta"></Header>
            </Modal.Header>
            <Modal.Description>
                <Divider />
                <Form loading={loading} onSubmit={() => submit(falta)}>
                    <Form.Field required>
                        <label>Data da falta</label>
                        <DateInput value={falta.data} dateFormat="DD/MM/YYYY" onChange={onChange} name="data" required />
                    </Form.Field>
                    <Form.Field required>
                        <label>Vendedor</label>
                        <SelectOperador required value={falta.operador_id} onChange={onChange} name="operador_id" />
                    </Form.Field>
                    <Form.Field>
                        <label>Justificativa (este campo não é obrigatório)</label>
                        <Form.TextArea name="justificativa" onChange={onChange} value={falta.justificativa}>
                        </Form.TextArea>
                    </Form.Field>
                    <Form.Button fluid content="Salvar falta" icon="save" color="black"></Form.Button>
                </Form>
            </Modal.Description>
        </Modal.Content>
    </Modal>
}

export default AddFaltaDialog