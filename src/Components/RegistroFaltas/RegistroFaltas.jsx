import React, { useState, useEffect } from "react";
import moment from "moment";
import Api from "../../Api";
import { Table, Button, Loader, Dimmer, Segment, Header } from "semantic-ui-react";
import { DatesRangeInput } from "semantic-ui-calendar-react";
import "moment/locale/pt-br";
import { useMemo, useContext } from "react";
import FilialContext from "../../Helpers/FilialContext";
import { useCallback } from "react";
import FormatarDinheiro from "../../Helpers/FormatarDinheiro";
import AddFaltaDialog from "./AddFaltaDialog";

const RegistroFaltas = (props) => {
  const { currentRole } = props;

  const filialContext = useContext(FilialContext);

  const [loading, setLoading] = useState(false);
  const [faltas, setFaltas] = useState([]);
  const [openNovo, setOpenNovo] = useState(false);

  const loadFaltas = useCallback(async () => {
    try {
      setLoading(true)
      const dados = await Api.Payroll.getFaltas();
      setFaltas(dados);
    } catch (err) {
      setFaltas([]);
    } finally{
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    loadFaltas();
  }, []);

  const onCloseDialog = useCallback(() => {
    setOpenNovo(false);
    loadFaltas()
  }, [loadFaltas])

  const apagarFalta = useCallback(async (idFalta) => {
    try {
      const result = window.confirm("Confirma a exclusão da falta?");
      if(result) {
        const del = await Api.Payroll.apagarFalta(idFalta)
        loadFaltas()
      }
    } catch(err) {

    }
  })

  return (
    <>
      <Header as="h1">
        <Header.Content>
          Faltas
          
        </Header.Content>
        <Button icon="add circle" content="Registrar falta" floated="right" color="black" onClick={() => setOpenNovo(true)} />
      </Header>
      <Segment loading={loading}>
        <Table>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Vendedor</Table.HeaderCell>
              <Table.HeaderCell>Data</Table.HeaderCell>
              <Table.HeaderCell>Justificativa</Table.HeaderCell>
              <Table.HeaderCell width={2} textAlign="right">Ações</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
              {faltas.map(falta => (
                  <Table.Row key={faltas._id}>
                      <Table.Cell>{falta.operador.name}</Table.Cell>
                      <Table.Cell>{moment(falta.data).format("DD/MM/YYYY")}</Table.Cell>
                      <Table.Cell>{falta.justificativa || 'Sem justificativa'}</Table.Cell>
                      <Table.Cell>
                        <Button fluid color="red" icon="trash" content="Apagar" onClick={() => apagarFalta(falta._id)}></Button>
                      </Table.Cell>
                  </Table.Row>
              ))}
          </Table.Body>
        </Table>
        <AddFaltaDialog open={openNovo} onClose={onCloseDialog} />
      </Segment>
    </>
  );
};

export default RegistroFaltas;
