import React, { useState, useEffect } from "react";
import moment from "moment";
import Api from "../../Api";
import { Table, Button, Loader, Dimmer, Segment, Header, Form } from "semantic-ui-react";
import { DatesRangeInput } from "semantic-ui-calendar-react";
import "moment/locale/pt-br";
import { useMemo, useContext } from "react";
import FilialContext from "../../Helpers/FilialContext";
import { useCallback } from "react";
import FormatarDinheiro from "../../Helpers/FormatarDinheiro";
import ConfirmarPagamento from "./ConfirmarPagamento";

const FolhaPagamento = (props) => {
  const { currentRole } = props;

  const filialContext = useContext(FilialContext);
  const [loading, setLoading] = useState(false);
  const [regrasEscalonamento, setRegrasEscalonamento] = useState([]);
  const [mesReferencia, setMesReferencia] = useState(
    moment().format("MM/YYYY")
  );

  const [mesAno, setMesAno] = useState({
    mes: moment().format('MM'),
    ano: moment().format("YYYY")
  })
  
  const updateMesReferencia = useCallback(() => {
    setMesReferencia(`${mesAno.mes}/${mesAno.ano}`)
  }, [mesAno])

  const [salarioBase, setSalarioBase] = useState(0);
  const [dadosVendedores, setDadosVendedores] = useState([]);

  const [dadosDialog, setDadosDialog] = useState(null);

  const loadEstatisticasVendedores = useCallback(async (mesReferencia) => {
    try {
      const dados = await Api.Payroll.vendasPorOperador(mesReferencia);
      setDadosVendedores(dados);
      const dadosEscalonamento = await Api.Payroll.getRules();
      setRegrasEscalonamento(dadosEscalonamento?.rules)
      const { value: SALARIO_BASE} = await Api.Provider.getParamentro('SALARIO_BASE')
      setSalarioBase(SALARIO_BASE)
    } catch (err) {
      setDadosVendedores([]);
    }
  }, []);

  useEffect(() => {
    loadEstatisticasVendedores(mesReferencia);
  }, [mesReferencia]);

  const onCloseDialog = useCallback(() => {
    setDadosDialog(null)
    loadEstatisticasVendedores(mesReferencia);
  }, [loadEstatisticasVendedores, mesReferencia])

  const dadosCalculados = useMemo(() => {
    const dados = dadosVendedores.map(dadosVendedor => {
        if(dadosVendedor.confirmado) {
          return dadosVendedor
        }
        const faixaAdicional = regrasEscalonamento.find(faixa => dadosVendedor.aprovadas > +faixa.min && dadosVendedor.aprovadas <= +faixa.max)
        if(faixaAdicional){
            dadosVendedor.adicional = dadosVendedor.aprovadas * faixaAdicional.valor
        }

        if(salarioBase) {
            dadosVendedor.salarioBase = +salarioBase
        }

        dadosVendedor.totalSalario = dadosVendedor.salarioBase + dadosVendedor.adicional

        return dadosVendedor
    })
    return dados
  }, [dadosVendedores, regrasEscalonamento, salarioBase])

  return (
    <Segment loading={loading}>
      <Form>
        <Form.Group inline>
          <Form.Field inline>
            <label>Mês</label>
            <Form.Input name="mes" value={mesAno.mes} onChange={(e, {name,value}) => setMesAno(mesAno => ({...mesAno, [name]: value}))}></Form.Input>
          </Form.Field>
          <Form.Field inline>
            <label>Ano</label>
            <Form.Input name="ano" value={mesAno.ano} onChange={(e, {name,value}) => setMesAno(mesAno => ({...mesAno, [name]: value}))}></Form.Input>
          </Form.Field>
          <Form.Field>
            <label>Buscar dados</label>
            <Form.Button icon="search" content="Buscar" color="black" fluid  onClick={updateMesReferencia}/>
          </Form.Field>
        </Form.Group>
      </Form>
      <Table striped celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Vendedor</Table.HeaderCell>
            <Table.HeaderCell>Num. Vendas</Table.HeaderCell>
            <Table.HeaderCell>Salário base</Table.HeaderCell>
            <Table.HeaderCell>Adicional vendas</Table.HeaderCell>
            <Table.HeaderCell>Faltas</Table.HeaderCell>
            <Table.HeaderCell>Total</Table.HeaderCell>
            <Table.HeaderCell width={1}>Confirmar</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
            {dadosCalculados.map(vendedor => (
                <Table.Row key={vendedor._id.operator}>
                    <Table.Cell>{vendedor._id.operator_name}</Table.Cell>
                    <Table.Cell>{vendedor.aprovadas}</Table.Cell>
                    <Table.Cell><FormatarDinheiro value={vendedor.salarioBase} tipo="text" /></Table.Cell>
                    <Table.Cell><FormatarDinheiro value={vendedor.adicional} tipo="text" /></Table.Cell>
                    <Table.Cell>{vendedor.faltas ?? 0}</Table.Cell>
                    <Table.Cell><FormatarDinheiro value={vendedor.totalSalario} tipo="text" /></Table.Cell>
                    <Table.Cell>
                      {!vendedor.confirmado && <Button content="Confirmar" icon="check circle" color="green" onClick={() => setDadosDialog(vendedor)}></Button>}
                      {vendedor.confirmado && <Header as="h4" icon="check circle" content="confirmado"></Header>}
                    </Table.Cell>
                </Table.Row>
            ))}
        </Table.Body>
      </Table>
      <ConfirmarPagamento registro={dadosDialog} onClose={onCloseDialog} />
    </Segment>
  );
};

export default FolhaPagamento;
