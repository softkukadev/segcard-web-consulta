import React, { useMemo } from 'react'
import { useCallback } from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import { Divider, Form, Header, Modal } from 'semantic-ui-react'
import SelectOperador from '../Selects/SelectOperador'
import {
    DateInput,
  } from 'semantic-ui-calendar-react';

import moment from 'moment'

import Api from '../../Api'
import FormatarDinheiro from '../../Helpers/FormatarDinheiro'

const ConfirmarPagamento = props => {
    const { registro, onClose } = props
    const [dados, setDados] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if(registro) {
            setDados({...registro})
        }
    }, [registro])

    const submit = useCallback(async (dados) => {
        try {
            setLoading(true);
            if(dados) {
                const ret = await Api.Payroll.confirmarFolha(dados)
                onClose();
            }
        } catch(error) { 

        } finally{
            setLoading(false);
        }
    }, [onClose])

    const onChange = useCallback((e, { name, value }) => {
        setDados(dados => ({ ...dados, [name]: value }))
    }, [])

    const open = useMemo(() => registro ?? false, [registro])
    
    if(!open) {
        return null
    }

    return <Modal open={open} onClose={onClose} closeIcon>
        <Modal.Content>
            <Modal.Header>
                <Header as="h1" content="Confirmação de folha de pagamento"></Header>
            </Modal.Header>
            <Modal.Description>
                <Divider />
                <Form loading={loading} onSubmit={() => submit(dados)}>
                    <Form.Field>
                        <label>Operador</label>
                        <Form.Input value={dados?._id?.operator_name} readOnly />
                    </Form.Field>
                    <Form.Field required>
                        <label>Salário base</label>
                        <FormatarDinheiro value={dados?.salarioBase} tipo="input" onChange={(valor) => onChange(null, {name: 'salarioBase', value: valor})} ></FormatarDinheiro>
                    </Form.Field>
                    <Form.Field required>
                        <label>Adicional vendas</label>
                        <FormatarDinheiro value={dados?.adicional} tipo="input" onChange={(valor) => onChange(null, {name: 'adicional', value: valor})} ></FormatarDinheiro>
                    </Form.Field>
                    <Form.Field required>
                        <label>Faltas</label>
                        <Form.Input required value={dados?.faltas} name="faltas" onChange={onChange} ></Form.Input>
                    </Form.Field>
                    <Form.Field required>
                        <label>Total</label>
                        <FormatarDinheiro value={dados?.totalSalario} tipo="input" onChange={(valor) => onChange(null, {name: 'totalSalario', value: valor})} ></FormatarDinheiro>
                    </Form.Field>
                    <Form.Button fluid content="Salvar falta" icon="save" color="black"></Form.Button>
                </Form>
            </Modal.Description>
        </Modal.Content>
    </Modal>
}

export default ConfirmarPagamento