import React, { Component, useState, useContext, useEffect } from "react";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import {
  Container,
  Icon,
  Menu,
  Responsive,
  Segment,
  Sidebar,
  Visibility,
  Header,
  Label,
  Dropdown,
} from "semantic-ui-react";
import routes from "../../Navigator/routes";
import ActionTypes from "../../Store/ActionTypes";
import { MAIN_COLOR } from "./../../environment";
import FilialContext from "../../Helpers/FilialContext";
import Api from "../../Api";
import AlterarSenha from "./AlterarSenha";
import { useMemo } from "react";
// Heads up!
// We using React Statrestic to prerender our docs with server side rendering, this is a quite simple solution.
// For more advanced usage please check Responsive docs under the "Usage" section.
const getWidth = () => {
  const isSSR = typeof window === "undefined";

  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth;
};

const seletor = state => state?.auth;

/* Heads up!
 * Neither Semantic UI nor Semantic UI React offer a responsive navbar, however, it can be implemented easily.
 * It can be more complicated, but you can create really flexible markup.
 */

function DesktopContainer(props) {
  const { children, currentRole, usuario, fullWidth } = props;
  const [fixed, setFixed] = useState(false);
  const hideFixedMenu = () => setFixed(false);
  const showFixedMenu = () => setFixed(true);

  const [alterarSenha, setAlterarSenha] = useState(false);

  const forcePasswordChange = useMemo(() => usuario?.data?.forcePasswordChange, [usuario])

  useEffect(() => {
    if (forcePasswordChange) {
      setAlterarSenha(true);
    }
  }, [forcePasswordChange])

  const [filialAtual, setFilialAtual] = useState(null)

  const idFilial = React.useMemo(() => usuario?.data?.idFilial, [usuario])

  useEffect(() => {
    async function findFilial(cancel, id) {
      try {
        const resp = await Api.Filial.list_all(cancel)
        if (Array.isArray(resp)) {
          setFilialAtual(resp.find(f => f._id === id))
        }
      } catch (error) {

      }
    }
    const cancel = Api.Filial.getCancelToken()
    if (idFilial) {
      findFilial(cancel, idFilial)
    }
    return () => cancel.cancel()
  }, [idFilial])

  const navigate = (path) => {
    props.history.push(path);
  };

  const filial = useContext(FilialContext)


  const fnOpenAlterarSenha = () => setAlterarSenha(true);
  const fnCloseAlterarSenha = () => setAlterarSenha(false);

  return (
    <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.minWidth}>
      <Visibility
        once={false}
        onBottomPassed={showFixedMenu}
        onBottomPassedReverse={hideFixedMenu}
      >
        <Segment textAlign="center" style={{ padding: "2em 0em" }} vertical>
          <Menu
            fixed="top"
            inverted={!fixed}
            color={!fixed ? MAIN_COLOR : null}
            size="huge"
          >
            <Container fluid>
              <img
                alt="teste"
                src={`${process.env.PUBLIC_URL}/logo.png`}
                style={{
                  display: fixed ? "none" : "block",
                  maxHeight: 60,
                  objectFit: "contain",
                  padding: ".5em",
                }}
              />
              <img
                alt="teste"
                src={`${process.env.PUBLIC_URL}/logo-alt.png`}
                style={{
                  display: !fixed ? "none" : "block",
                  maxHeight: 60,
                  objectFit: "contain",
                  padding: ".5em",
                }}
              />
              {filial.isFilial && <Label color="black" inverted pointing="left" size="huge" style={{ display: 'flex', alignItems: 'center' }}>{filialAtual?.nomeFilial?.toUpperCase()}</Label>}
              <Link to={routes.INICIO}>
                <Menu.Item
                  as="span"
                  style={{ width: "100%", height: "100%" }}
                  link
                >
                  <Icon name="search" />
                      Consulta
                    </Menu.Item>
              </Link>

            </Container>
            <Dropdown style={{ display: 'flex', alignSelf: 'center', color: !fixed ? 'white' : 'black' }} text={usuario.data.name}>
              <Dropdown.Menu>
                <Dropdown.Item icon="key" text="Alterar senha" onClick={fnOpenAlterarSenha}></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Menu.Item
              as="a"
              onClick={props.logout}
              content="Sair"
              icon="sign out"
              position="right"
            ></Menu.Item>
          </Menu>
        </Segment>
      </Visibility>

      <div>{children}</div>
      <AlterarSenha open={alterarSenha} onClose={fnCloseAlterarSenha} />
    </Responsive>
  )
}

class MobileContainer extends Component {
  state = {};

  handleSidebarHide = () => this.setState({ sidebarOpened: false });

  handleToggle = () => this.setState({ sidebarOpened: true });

  navigate = (path) => {
    this.props.history.push(path);
  };

  render() {
    const { children, logout } = this.props;
    const { sidebarOpened } = this.state;

    return (
      <Responsive
        as={Sidebar.Pushable}
        getWidth={getWidth}
        maxWidth={Responsive.onlyMobile.maxWidth}
      >
        <Sidebar
          as={Menu}
          animation="push"
          onHide={this.handleSidebarHide}
          vertical
          visible={sidebarOpened}
        >
          <Menu.Item onClick={() => logout()} as="a">
            <Icon name="sign out" />
            Sair
          </Menu.Item>
        </Sidebar>

        <Sidebar.Pusher dimmed={sidebarOpened}>
          <Segment textAlign="center" style={{ padding: "1em 0em" }} vertical>
            <Container>
              <Menu color={"violet"} pointing size="large">
                <Menu.Item onClick={this.handleToggle}>
                  <Icon name="sidebar" />
                </Menu.Item>
                <img
                  alt="teste"
                  src={`${process.env.PUBLIC_URL}/images/logo-alt.png`}
                  style={{
                    maxHeight: 50,
                    objectFit: "contain",
                    padding: ".5em",
                  }}
                />
                <Menu.Item position="right"></Menu.Item>
              </Menu>
            </Container>
          </Segment>
          {children}
        </Sidebar.Pusher>
      </Responsive>
    );
  }
}

const ResponsiveContainer = ({
  children,
  history,
  fullWidth,
  auth: usuario,
  logout,
  ...others
}) => {
  /*const logout = () => {
    AuthApi.signout();
    history.push(ROUTES.LOGIN);
  };*/

  const [open, setOpen] = useState(true);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <div className="content-wrapper-b">
        <DesktopContainer
          fullWidth={fullWidth}
          history={history}
          usuario={usuario}
          logout={logout}
          {...others}
        >
          <Container style={{ paddingTop: "1em", width: "95%" }}>
            {children}
          </Container>
        </DesktopContainer>
        <MobileContainer
          history={history}
          usuario={usuario}
          logout={logout}
          {...others}
        >
          <Container style={{ paddingTop: "1em" }}>{children}</Container>
        </MobileContainer>
        <div className="push" />
      </div>
    </>
  );
};

const mapStateToProps = ({ auth }) => ({ auth });
const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch({ type: ActionTypes.AUTH.LOGOUT }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ResponsiveContainer));
