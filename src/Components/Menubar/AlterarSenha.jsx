import React, { useState, useEffect } from 'react';
import { Modal, Form, Button, Message } from 'semantic-ui-react';
import { MAIN_COLOR } from '../../environment';
import Api from '../../Api';
import { useDispatch } from 'react-redux';
import ActionTypes from '../../Store/ActionTypes';

const ESTADO_INICIAL = {
    password: '',
    newPassword: '',
    passwordRepeat: '',
}

const AlterarSenha = props => {
    const { open, onClose } = props;

    const [form, setForm] = useState({...ESTADO_INICIAL})
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");

    const changeForm = (event, { name, value }) => {
        setForm(form => ({...form, [name]: value}))
    }

    const dispatch = useDispatch();

    const submit = async () => {
        try {
            setLoading(true);
            if(form.newPassword !== form.passwordRepeat) {
                setError("Nova senha não confere com a repetição");
            }
            const ret = await Api.Auth.changePassword(form);
            dispatch({ type: ActionTypes.AUTH.CHANGE_PASSWORD })

            onClose()
        } catch(error){
            setError(error?.response?.data?.error);
        } finally{
            setLoading(false)
        }
    }

    useEffect(() => {
        if(open) {
            setForm({...ESTADO_INICIAL})
        }
    }, [open])

    return <Modal open={open} closeIcon onClose={onClose}>
        <Modal.Header>
            Alteração de senha
        </Modal.Header>
        <Modal.Content>
            <Form loading={loading} error={error ? true : false} onSubmit={submit}>
                <Form.Field required>
                    <label>Senha atual</label>
                    <Form.Input required name="password" type="password" onChange={changeForm} />
                </Form.Field>

                <Form.Field required>
                    <label>Nova senha</label>
                    <Form.Input required name="newPassword" type="password" onChange={changeForm} />
                </Form.Field>

                <Form.Field required>
                    <label>Repita a nova senha</label>
                    <Form.Input required name="passwordRepeat" type="password" onChange={changeForm} />
                </Form.Field>

                <Message error>{error}</Message>

                <Button fluid type="submit" content="Alterar senha" color={MAIN_COLOR}  />
            </Form>
        </Modal.Content>
    </Modal>
}

export default AlterarSenha