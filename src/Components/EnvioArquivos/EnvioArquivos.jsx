import moment from "moment";
import Payment from "payment";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useDropzone } from "react-dropzone";
import Moment from "react-moment";
import { connect } from "react-redux";
import {
  Button,
  Dimmer,
  Form,
  Header,
  Icon,
  Loader,
  Message,
  Table,
  Checkbox,
  Placeholder,
  Segment,
} from "semantic-ui-react";
import styled from "styled-components";
import Api from "../../Api";
import { MAIN_COLOR } from "../../environment";
import leituraCsvLote, { renameFields } from "../../Helpers/leituraCsvLote";
import SelectOperador from "../Selects/SelectOperador";

const DragDropZone = styled.div`
  padding: 20px;
  width: 100%;
  border-radius: 5px;
  border: 2px solid #ccc;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;

const EnvioArquivos = (props) => {
  const [arquivos, setArquivos] = useState([]);
  const [dados, setDados] = useState([]);
  const [erro, setErro] = useState("");
  const [suprimidos, setSuprimidos] = useState(0);
  const [loading, setLoading] = useState(false);
  const [success, setSucess] = useState(false);
  const [codigoLote, setCodigoLote] = useState("");
  const [numErros, setNumErros] = useState(0);
  const [gerarMovimentos, setGerarMovimentos] = useState(false);
  const [vendedor, setVendedor] = useState(null);

  const { provider } = props;

  const load = useCallback(async () => {
    await Api.Provider.get_current();
  });

  const submit = useCallback(
    async (gerarMovimentos, vendedor, dados) => {
      if (provider) {
        setSucess(false);
        setLoading(true);
        setErro("");
        setNumErros(0);
        try {
          const ret = await Api.Sell.new_sale_array(provider.slug, { gerarMovimentos: gerarMovimentos, vendedor, dados: dados });
          if (Array.isArray(ret.resp) && ret.resp.length > 0) {
            if(ret.resp[0].codigoLote) {
              setCodigoLote(ret.resp[0].codigoLote)
            }
            const notSuccess = ret.resp.filter((c) => !c.success);
            setNumErros(notSuccess.length);
          }
          setDados([]);
          setSucess(true);
        } catch (error) {
          setSucess(false);
          if (error.response && error.response.data) {
            setErro(error.response.data.erro);
          } else {
            setErro(error.message);
          }
        } finally {
          setLoading(false);
        }
      }
    },
    [provider]
  );

  const changeVendedor = (e, { name, value }) => {
    setVendedor(value);
  } 


  useEffect(() => {
    load();
  }, []);

  const onDrop = useCallback((acceptedFiles) => {
    setErro("");
    acceptedFiles.forEach((file) => {
      if (!(file.name && file.name.toLowerCase().endsWith("csv"))) {
        setErro("Arquivo em formato inválido");
        return;
      }
      const reader = new FileReader();

      reader.onloadend = async (e) => {
        try {
          const data = await leituraCsvLote(e.target.result);
          let dadosNomeados = renameFields(data, {
            Nome: "name",
            CPF: "cpf",
            "Data nascimento": "birthdate",
            Telefone: "phone_number",
            Email: "email",
            Cartao: "card",
            Validade: "expires",
            cep: "cep",
            logradouro: 'logradouro',
            numero: 'numero',
            bairro: 'bairro',
            cidade: 'cidade',
            estado: 'estado'
          });

          dadosNomeados = dadosNomeados.map(item => {
            const { cep, logradouro, numero, bairro, cidade, estado, ...resto} = item;
            if(cep && logradouro) {
              resto.endereco = {
                cep, logradouro, numero, bairro, cidade, estado
              }
            }
            return resto
          });

          setDados((dados) => {
            let suprimidos = 0;
            const novosDados = [...dados];
            dadosNomeados.forEach((item) => {
              if (
                !dados.find(
                  (cli) => cli.cpf === item.cpf && cli.card === item.card
                )
              ) {
                //Não possui duplicados, verifica se é valido
                let valid = true;
                // item.card = item.card.trim();
                // console.log(item.card, Payment.fns.validateCardExpiry(item.expires), Payment.fns.validateCardNumber(item.card))
                valid = valid && Payment.fns.validateCardExpiry(item.expires);
                //valid = valid && Payment.fns.validateCardNumber(item.card.trim());

                // console.log(valid);
                const birthdate = moment(item.birthdate, "DD/MM/YYYY").toDate();

                novosDados.push({ ...item, valid, birthdate });
              } else {
                suprimidos += 1;
              }
            });
            setSuprimidos(suprimidos);
            return novosDados;
          });
        } catch (erro) {
          const index = arquivos.indexOf(file);
          if (index > -1) {
            arquivos.splice(index, 1);
          }
        }
      };
      reader.readAsText(file);
      arquivos.push(file);
    });

    // Do something with the files
  }, []);

  const removerItemLista = useCallback((cpf) => {
    setDados((dados) => dados.filter((c) => c.cpf !== cpf));
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  const hasInvalid = useMemo(() => dados.some((c) => !c.valid), [dados]);
  const hasValid = useMemo(() => dados.some((c) => c.valid), [dados]);

  return (
    <>
      <Segment textAlign="center" tertiary>
          <Header icon >
          <Icon name='excel file outline' />
          Baixe o template para a importação no botão abaixo

        </Header><br/>
        <Button primary as="a" circular target="_blank" href="/template.csv">Baixar template</Button>
      </Segment>
      <Form>
        <DragDropZone {...getRootProps()}>
          <input {...getInputProps()} />
          {isDragActive ? (
            <Header color={MAIN_COLOR}>
              <Header.Content>
                <Icon name="cloud upload" size="huge" />
                Solte os arquivos para fazer o envio
              </Header.Content>
            </Header>
          ) : (
            <Header color={MAIN_COLOR}>
              <Header.Content>
                <Icon name="add circle" size="huge" />
                Arraste arquivos ou clique para selecionar
              </Header.Content>
            </Header>
          )}
        </DragDropZone>
        <Form.Group>
          <Form.Field style={{marginTop: '1em'}}>
            <label>Gerar Movimentos</label>
            <Checkbox toggle checked={gerarMovimentos} label="Gerar movimentos para a importação (Adiciona para os contadores)*" onChange={(e,t) => setGerarMovimentos(t.checked)} />
          </Form.Field>

          <Form.Field width={5} style={{marginTop: '1em'}}>
            <label>Operador</label>
            <SelectOperador name="vendedor" onChange={changeVendedor} value={vendedor} />
          </Form.Field>
        </Form.Group>
        {suprimidos > 0 && (
          <Header color="red">
            {suprimidos} registros suprimidos por duplicidade de CPF e/ou Cartão
          </Header>
        )}
        {hasInvalid && (
          <Header
            as="h3"
            color="red"
            content="Alguns registros incluem cartões inválidos ou vencidos. Em vermelho na listagem abaixo."
          />
        )}
        <Message size="huge" visible={erro ? true : false} error>
          {erro}
        </Message>
        <Message
          size="huge"
          visible={success ? true : false}
          icon="check circle"
          content={`Transações realizadas - CODIGO DO LOTE: ${codigoLote}`}
          success
        ></Message>
        <Message
          size="large"
          visible={numErros > 0 ? true : false}
          icon="warning circle"
          content={`${numErros} transações tiveram erro no processamento, verifique no menu falhas`}
          warning
        ></Message>
        <Table padded selectable color={MAIN_COLOR}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Nome do cliente</Table.HeaderCell>
              <Table.HeaderCell>CPF</Table.HeaderCell>
              <Table.HeaderCell>Dt. Nasc.</Table.HeaderCell>
              <Table.HeaderCell>Email</Table.HeaderCell>
              <Table.HeaderCell>Telefone</Table.HeaderCell>
              <Table.HeaderCell>Cartão</Table.HeaderCell>
              <Table.HeaderCell>Validade</Table.HeaderCell>
              <Table.HeaderCell>Endereço</Table.HeaderCell>
              <Table.HeaderCell width={1}>Remover</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {dados.map((arquivo) => {
              return (
                <Table.Row
                  key={arquivo.cpf}
                  className={!arquivo.valid && "invalid-row"}
                >
                  <Table.Cell>{arquivo.name}</Table.Cell>
                  <Table.Cell>{arquivo.cpf}</Table.Cell>
                  <Table.Cell>
                    <Moment format="DD/MM/YYYY">{arquivo.birthdate}</Moment>
                  </Table.Cell>
                  <Table.Cell>{arquivo.email}</Table.Cell>
                  <Table.Cell>{arquivo.phone_number}</Table.Cell>
                  <Table.Cell>{arquivo.card}</Table.Cell>
                  <Table.Cell>{arquivo.expires}</Table.Cell>
                  <Table.Cell>{arquivo?.endereco?.cep} - {arquivo?.endereco?.logradouro}</Table.Cell>
                  <Table.Cell>
                    <Button
                      onClick={() => {
                        removerItemLista(arquivo.cpf);
                      }}
                      icon="trash"
                      color="red"
                    />
                  </Table.Cell>
                </Table.Row>
              );
            })}
          </Table.Body>
        </Table>
        <Form.Button
          disabled={!hasValid || loading}
          onClick={() => submit(gerarMovimentos, vendedor, dados)}
          color={MAIN_COLOR}
          content="Realizar cobranças"
          icon="send"
          fluid
          size="massive"
        />
      </Form>
      <Dimmer active={loading} inverted>
        <Loader>Realizando operações</Loader>
      </Dimmer>
    </>
  );
};

const mapStateToProps = (state) => ({
  provider: state.provider,
});

export default connect(mapStateToProps)(EnvioArquivos);
