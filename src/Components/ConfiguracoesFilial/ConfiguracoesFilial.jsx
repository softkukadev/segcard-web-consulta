import React, { useEffect, useState, useCallback } from 'react'
import { Header, Modal, Form, Button, Divider } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import Api from '../../Api';
import { useMemo } from 'react';
import FormatarDinheiro from '../../Helpers/FormatarDinheiro';

const ConfiguracoesFilial = props => {
    const { filial, onClose } = props;
    const [providers, setProviders] = useState([])
    const [seguradoras, setSeguradoras] = useState([])
    const [form, setForm] = useState({
        seguradora: '',
        provider: '',
        overrides: {},
        provider_recorrencia: '',
        overrides_recorrencia: '',
        salario_base: '',
    });
    const [loadingSave, setLoadingSave] = useState(false);
    const [providerChanged, setProviderChanged] = useState(false);
    const [providerRecorrenciaChanged, setProviderRecorrenciaChanged] = useState(false);

    const changeForm = React.useCallback((event, target) => {
        const { name, value } = target;
        setForm(form => ({...form, [name]: value}))
        if(name === 'provider'){
            setProviderChanged(true);
        }
        if(name === 'provider_recorrencia') {
            setProviderRecorrenciaChanged(true)
        }
    })

    const changeOverrides = React.useCallback((event, target) => {
        const { name, value } = target;
        setForm(form => ({...form, overrides: { ...form.overrides, [name]: value }}))
    })

    const changeOverridesRecorrencia = React.useCallback((event, target) => {
        const { name, value } = target;
        setForm(form => ({...form, overrides_recorrencia: { ...form.overrides_recorrencia, [name]: value }}))
    })

    useEffect(() => {
        async function loadProviders(cancel) {
            try {
                const providers = await Api.Provider.list_providers(cancel);
                setProviders(providers);
                const seguradoras = await Api.Seguradora.list_seguradoras(cancel);
                setSeguradoras(seguradoras)
            } catch(error) {
                setProviders([])
            }
        }
        const cancel = Api.Provider.getCancelToken()
        loadProviders(cancel)
        return () => cancel.cancel()
    }, [])

    const optionsProviders = useMemo(() => providers.map(prov => ({ text: prov.name, value: prov._id, key: prov._id })), [providers])
    const optionsSeguradoras = useMemo(() => seguradoras.map(prov => ({ text: prov.name, value: prov._id, key: prov._id })), [seguradoras])

    const { provider, provider_recorrencia } = form;
    const providerSelecionado = useMemo(() => providers.find(p => p._id === provider) ?? null, [provider, providers])

    const providerRecSelecionado = useMemo(() => providers.find(p => p._id === provider_recorrencia) ?? null, [provider_recorrencia, providers])

    useEffect(() => {
        if(providerSelecionado && providerChanged) {
            setForm(form => ({
                ...form,
                overrides: {...providerSelecionado.credentials}
            }))
        }
    }, [providerSelecionado, providerChanged])

    useEffect(() => {
        if(providerRecSelecionado && providerRecSelecionado) {
            setForm(form => ({
                ...form,
                overrides_recorrencia: {...providerRecSelecionado.credentials}
            }))
        }
    }, [providerRecSelecionado, providerRecorrenciaChanged])

    useEffect(() => {
        if(filial) {
            setProviderChanged(false);
            setForm({
                provider: filial.provider ?? '',
                seguradora: filial.seguradora ?? '',
                overrides: filial.overrides ?? {},
                provider_recorrencia: filial.provider_recorrencia ?? '',
                overrides_recorrencia: filial.overrides_recorrencia ?? {},
            })
        }
    }, [filial])

    const submit = useCallback(async (id, data) => {
        setLoadingSave(true);
        try {
            const ret = await Api.Filial.alterar_provider(id, data);
            onClose();
        } catch(error) {
            console.log(error)
        }
        setLoadingSave(false)
    }, [onClose]);

    if(!filial) {
        return null
    }
    

    return <Modal open={filial !== null} closeIcon onClose={onClose}>
        <Modal.Header>
            <Header as="h2">Configurações {filial.nomeFilial}</Header>
        </Modal.Header>
        <Modal.Content>
            <Form loading={loadingSave} onSubmit={() => submit(filial._id, form)}>
                <Form.Field>
                    <label>Seguradora</label>
                    <Form.Select name="seguradora" value={form.seguradora} multiple onChange={changeForm} options={optionsSeguradoras}></Form.Select>
                </Form.Field>
                <Form.Field>
                    <label>Salário base</label>
                    <FormatarDinheiro value={form.salario_base} onChange={(valor) => changeForm(null, {name: 'salario_base', value: valor})} tipo="input" />
                </Form.Field>
                <Divider content="Novas vendas" section horizontal/>
                <Form.Field>
                    <label>Provider</label>
                    <Form.Select name="provider" value={form.provider} onChange={changeForm} options={optionsProviders}></Form.Select>
                </Form.Field>
                <Header as="h5" content="Override de configurações" />
                    {
                        providerSelecionado && Object.keys(form.overrides).map(key => (
                            <Form.Field key={key}>
                                <label>{key}</label>
                                <Form.Input value={form.overrides[key]} name={key} onChange={changeOverrides} />
                            </Form.Field>
                        ))
                    }

                <Divider content="Recorrencia" section horizontal/>
                <Form.Field>
                    <label>Provider de recorrência</label>
                    <Form.Select name="provider_recorrencia" value={form.provider_recorrencia} onChange={changeForm} options={optionsProviders}></Form.Select>
                </Form.Field>
                <Header as="h5" content="Override de configurações / Recorrência" />
                    {
                        providerRecSelecionado && Object.keys(form.overrides_recorrencia).map(key => (
                            <Form.Field key={key}>
                                <label>{key}</label>
                                <Form.Input value={form.overrides_recorrencia[key]} name={key} onChange={changeOverridesRecorrencia} />
                            </Form.Field>
                        ))
                    }
                
                    <Button icon="save" content="Salvar" fluid />
            </Form>
        </Modal.Content>
    </Modal>
}

ConfiguracoesFilial.propTypes = {
    filial: PropTypes.object,
    onClose: PropTypes.func.isRequired,
}

export default ConfiguracoesFilial;