import React, { useState, useEffect } from 'react'
import { Table, Button, Form, Dimmer, Loader } from 'semantic-ui-react';
import Api from '../../Api';
import { MAIN_COLOR } from '../../environment';

const LinhaParametro = ({parametro}) => {
    const [editando, setEditando] = useState(false)
    const [form, setForm] = useState({...parametro})

    const change = (e, {name, value}) => {
        setForm({ ...form, [name]: value })
    }

    const save = async () => {
        try{
            const parameter = await Api.Parameters.update_provider(parametro._id, form)
            setEditando(false);
        } catch(erro) {
            setEditando(false)
            setForm({...parametro})
        }
    }
    

    return (
        <Table.Row>
            <Table.Cell>
                { form.name }
            </Table.Cell>
            <Table.Cell>
                { form.type }
            </Table.Cell>
            <Table.Cell>
                { !editando && form.value }
                {  editando && <Form.Input value={form.value} name="value" onChange={change} placeholder="Valor" /> }
            </Table.Cell>
            <Table.Cell textAlign="right">
                { !editando && <Button color={MAIN_COLOR} basic fluid icon="edit" onClick={() => setEditando(true)} content="Editar"/> }
                {  editando && <Button color={MAIN_COLOR} fluid icon="save" onClick={save} content="Salvar"/> }
            </Table.Cell>
        </Table.Row>
    )
}

const Parametros = props => {
    const [parametros, setParametros] = useState([])
    const [loading, setLoading] = useState(false);

    async function load(){
        setLoading(true)
        try{
            const params = await Api.Parameters.get()
            setParametros(params)
        } catch(error){

        }
        setLoading(false);
    }

    useEffect(() => {
        load()
    }, [])

    return (
        <>
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width={5}>Nome</Table.HeaderCell>
                        <Table.HeaderCell width={3}>Tipo</Table.HeaderCell>
                        <Table.HeaderCell>Valor</Table.HeaderCell>
                        <Table.HeaderCell width={3} textAlign="right">Editar</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {parametros.map(parametro => <LinhaParametro key={parametro._id} parametro={parametro} />)}
                </Table.Body>
            </Table>
            <Dimmer inverted active={loading}>
                <Loader>Carregando parametros</Loader>
            </Dimmer>
        </>
    )
    
}

export default Parametros