import React, { useState } from 'react'
import { Header } from 'semantic-ui-react'
import ListaVendas from '../ListaVendas/ListaVendas'

const Assinaturas = (props) => {
    const [refresh, setRefresh] = useState(1);
    
    const callback = () => setRefresh(refresh + 1)

    return (
        <>
            <Header as="h2" attached="top">
                <Header.Content>
                    Assinaturas
                </Header.Content>
            </Header>
            <ListaVendas callback={callback} refresh={refresh} {...props} attached padded />
        </>
    )
}
  
export default Assinaturas