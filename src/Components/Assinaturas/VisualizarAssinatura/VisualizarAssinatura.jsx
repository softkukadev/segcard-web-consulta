import moment from "moment";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Divider, Form, Header, Segment, Table, Button, Confirm, Message, Label } from "semantic-ui-react";
import Api from "../../../Api";
import { baseURL } from './../../../Api/api'

const VisualizarAssinatura = (props) => {
  const { id } = useParams();

  const [loading, setLoading] = useState(false);
  const [assinatura, setAssinatura] = useState(null);
  const [historico, setHistorico] = useState([]);
  const [erroEstorno, setErroEstorno] = useState('');
  const [documentos, setDocumentos] = useState([]);

  const [vendaEstorno, setVendaEstorno] = useState(null);
  const [erroCancelamentoApolice, setErroCancelamentoApolice] = useState('');

  async function loadAssinatura(cancelToken, id) {
    setLoading(true);
    try {
      const data = await Api.Sell.sale_por_id(id, cancelToken);
      setAssinatura(data);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (id) {
      const cancel = Api.Sell.getCancelToken();
      loadAssinatura(cancel, id);
      return () => cancel.cancel("");
    }
  }, [id]);

  useEffect(() => {
    async function loadPagamentos(cancelToken, id) {
      setLoading(true);
      try {
        const data = await Api.Sell.pagamentos_por_id(id, cancelToken);
        setHistorico(data);
        const documentos = await Api.Sell.documentos_assinatura(id, cancelToken);
        setDocumentos(documentos)
      } catch (error) {
        setHistorico([]);
      } finally {
        setLoading(false);
      }
    }
    if (assinatura) {
      const cancel = Api.Sell.getCancelToken();
      loadPagamentos(cancel, id);
      return () => cancel.cancel("");
    }
  }, [assinatura]);

  const unsubscribe = async (provider, id) => {
    try {
      setLoading(true);
      const unsub = await Api.Sell.cancel_sale(provider, id);      
      const cancel = Api.Sell.getCancelToken();
      loadAssinatura(cancel, id)

    } catch (erro) {
      
    } finally{
      setLoading(false);
    }
  };

  const abrir_documento = async (id) => {
    const { link } = await Api.Sell.get_link_documento(id);
    window.open(link, '_blank');
  }

  const estornar_pagamento = async (id_pagamento) => {

    async function loadPagamentos(cancelToken, id) {
      setLoading(true);
      try {
        const data = await Api.Sell.pagamentos_por_id(id, cancelToken);
        setHistorico(data);
      } catch (error) {
        setHistorico([]);
      } finally {
        setLoading(false);
      }
    }

    setVendaEstorno(null);
    setErroEstorno('');
    setLoading(true);
      try {
        const req = await Api.Sell.estornar_pagamento(assinatura.provider, id_pagamento);
        console.log(req);
        setErroEstorno("Estorno solicitado, status encontra-se em: "+req?.transactionResponse?.state);
      } catch (error) {
        setErroEstorno(error?.response?.data?.error);
        //console.log(error?.response?.data?.error);
        //console.log(error?.response?.data);
      } finally {
        setLoading(false);
        const cancel = Api.Sell.getCancelToken();
        loadPagamentos(cancel, id);
      }
  }

  const cancelarApolice = async (id, seguradora) => {
    try{
      setLoading(true);
      const ret = await Api.Seguradora.cancelar_seguro(id, seguradora);
      const cancel = Api.Seguradora.getCancelToken()
      loadAssinatura(cancel, id);
    } catch(error) {
      if(error?.response?.data?.error) {
        setErroCancelamentoApolice(error?.response?.data?.error)
      } else {
        setErroCancelamentoApolice(error.message)
      }
    } finally{
      setLoading(false)
    }
  }

  return (
    <>
      <Header attached="top" as="h2">
        <Header.Content>Visualizando assinatura</Header.Content>
        {assinatura?.status !== "suspended" && <Header.Content style={{float: 'right'}}>
          <Button color="red" onClick={() => unsubscribe(assinatura.provider, id)} compact content="Cancelar assinatura" icon="times" />
        </Header.Content>}
      </Header>
      <Segment attached="bottom" loading={loading}>
        {assinatura && (
          <>
            <Form error={erroEstorno ? true : false}>
              <Form.Group>
                <Form.Field width="12">
                  <label>Nome</label>
                  <Form.Input readOnly transparent value={assinatura.name} />
                </Form.Field>
                <Form.Field width="4">
                  <label>CPF</label>
                  <Form.Input readOnly transparent value={assinatura.cpf} />
                </Form.Field>
              </Form.Group>

              <Form.Group>
                <Form.Field width="7">
                  <label>Email</label>
                  <Form.Input readOnly transparent value={assinatura.email} />
                </Form.Field>
                <Form.Field width="5">
                  <label>Telefone</label>
                  <Form.Input
                    readOnly
                    transparent
                    value={assinatura.phone_number}
                  />
                </Form.Field>
                <Form.Field width="4">
                  <label>Data nascimento</label>
                  <Form.Input
                    readOnly
                    transparent
                    value={moment(assinatura.birthdate).format("DD/MM/YYYY")}
                  />
                </Form.Field>
              </Form.Group>

              <Form.Group>
                <Form.Field width="7">
                  <label>Cartão</label>
                  <Form.Input readOnly transparent value={assinatura.card} />
                </Form.Field>
                <Form.Field width="5">
                  <label>Validade</label>
                  <Form.Input
                    readOnly
                    transparent
                    value={assinatura.expiration}
                  />
                </Form.Field>
                <Form.Field width="4">
                  <label>Data da assinatura</label>
                  <Form.Input
                    readOnly
                    transparent
                    style={{ fontWeight: "bold", fontSize: 14, color: "red" }}
                    value={moment(assinatura.created_at).format("DD/MM/YYYY")}
                  />
                </Form.Field>
              </Form.Group>
              <Message error content={erroEstorno}></Message>
            </Form>
            <Divider />
            { assinatura.apolice && <>
              <Header as="h4" content="Apólices"/>
              { assinatura.apolice.map(apolice => {
                return <Segment clearing>
                  <strong>{apolice.seguradora}</strong>
                  {apolice.seguradora === 'topmed' && <>
                    <span style={{display:'inline-block', marginLeft: 10}}>Status: <Label color={apolice?.data?.request?.StatusBeneficiario === 'A' ? 'green' : 'red'}>{ apolice?.data?.request?.StatusBeneficiario === 'A' ? 'Ativo' : 'Inativo' }</Label></span>
                    { apolice?.data?.request?.StatusBeneficiario === 'A' && <Button size="small" floated="right" onClick={() => cancelarApolice(id, apolice.seguradora)} disabled={loading} loading={loading} color="red" content="Cancelar apólice" icon="times circle"/>}
                  </>}
                  {apolice.seguradora === 'sulamerica' && <>
                    <span style={{display:'inline-block', marginLeft: 10}}>Status: <Label color={apolice?.data?.status !== 'SUSPENDED' ? 'green' : 'red'}>{ apolice?.data?.status !== 'SUSPENDED' ? 'Ativo' : 'Inativo' }</Label></span>
                    { apolice?.data?.status !== 'SUSPENDED' && <Button size="small" floated="right" onClick={() => cancelarApolice(id, apolice.seguradora)} disabled={loading} loading={loading} color="red" content="Cancelar apólice" icon="times circle"/> }
                  </>}
                </Segment>
              }) }
            </> }
            <Divider/>
            {assinatura.status === "suspended" && (
              <Header
                color="red"
                as="h3"
                content="A assinatura está suspensa."
              />
            )}
            {}
            {assinatura.status !== "suspended" && (
              <Header
                color="green"
                as="h3"
                content={`Proxima cobrança será dia ${assinatura.proximaCobranca}`}
              />
            )}
            <Header as="h4" content="Movimentos da assinatura" />
            <Table>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell width={3}>Data</Table.HeaderCell>
                  <Table.HeaderCell>Tipo</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {assinatura?.movimentos?.map((movimento) => (
                  <Table.Row key={movimento._id}>
                    <Table.Cell>
                      {moment(movimento.date).format("DD/MM/YYYY HH:mm")}
                    </Table.Cell>
                    <Table.Cell>{movimento.canceled ? 'Cancelamento' : "Assinatura"}</Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
            <Header as="h3" content="Histórico de pagamentos" />
            <Table>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell width={3}>Data</Table.HeaderCell>
                  <Table.HeaderCell>Id transação</Table.HeaderCell>
                  <Table.HeaderCell>Status</Table.HeaderCell>
                  <Table.HeaderCell>Valor</Table.HeaderCell>
                  { assinatura.provider !== 'pagseguro' && <Table.HeaderCell textAlign="right">Estornar</Table.HeaderCell> }
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {historico.map((pagamento) => (
                  <Table.Row key={pagamento._id} style={{backgroundColor: pagamento.status === 'FAILED' ? 'rgba(186, 101, 115, .4)' : 'rgba(101, 186, 115, .4)' }}>
                    <Table.Cell>
                      {moment(pagamento.created_at).format("DD/MM/YYYY HH:mm")}
                    </Table.Cell>
                    <Table.Cell>{pagamento.id_transacao}</Table.Cell>
                    <Table.Cell>{pagamento.status} {pagamento.status === 'FAILED'  && `${pagamento?.response?.transactionResponse?.paymentNetworkResponseCode ?? ''}: ${pagamento?.response?.transactionResponse?.paymentNetworkResponseErrorMessage} `}</Table.Cell>
                    <Table.Cell>{Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(pagamento.valor  )}</Table.Cell>
                    { assinatura.provider !== 'pagseguro' && <Table.Cell textAlign="right">
                      {!pagamento.estorno ?
                        <Button onClick={() => setVendaEstorno(pagamento)} color='orange' content="Estornar" icon="undo" compact />
                        :
                        <Header as="h4" content="Estornado" />}
                    </Table.Cell>}
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>

            <Header as="h4" content="Documentos" />
            <Table>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell width={3}>Data</Table.HeaderCell>
                  <Table.HeaderCell>Seguradora</Table.HeaderCell>
                  <Table.HeaderCell>Tipo</Table.HeaderCell>
                  <Table.HeaderCell textAlign="right">Abrir</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {documentos.map((documento) => (
                  <Table.Row key={documento._id}>
                    <Table.Cell>
                      {moment(documento.created_at).format("DD/MM/YYYY HH:mm")}
                    </Table.Cell>
                    <Table.Cell>{documento.seguradora}</Table.Cell>
                    <Table.Cell>{documento.tipo}</Table.Cell>
                    <Table.Cell textAlign="right">
                      <Button  content="Abrir" onClick={() => abrir_documento(documento._id)} />
                    </Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
            
            
            <Confirm
              content={vendaEstorno && `Confirma o estorno do pagamento do dia ${moment(vendaEstorno.created_at).format("DD/MM/YYYY HH:mm")} `}
              cancelButton="Cancelar"
              confirmButton="Confirmar estorno"
              open={vendaEstorno !== null}
              onCancel={() => setVendaEstorno(null)}
              onConfirm={() => estornar_pagamento(vendaEstorno?._id)}
            />  
          </>
        )}
      </Segment>
    </>
  );
};

export default VisualizarAssinatura;
