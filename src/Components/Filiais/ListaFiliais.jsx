import React, { useState, useEffect } from 'react'
import { Table, Dimmer, Loader, Pagination, Dropdown, Menu, Icon, Segment, Form, Header, Button } from 'semantic-ui-react'
import Api from '../../Api'
import { isFunction } from 'util';
import { MAIN_COLOR } from '../../environment';
import { Link } from 'react-router-dom';
import { USUARIOS, USUARIOS_FILIAIS } from '../../Navigator/routes';
import ConfiguracoesFilial from '../ConfiguracoesFilial/ConfiguracoesFilial';

const ListaFiliais = props => {
    const { shouldRefresh, refresh } = props;
    const [loading, setLoading] = useState(false);
    const [filiais, setFiliais] = useState(null);
    const [pagina, setPagina] = useState(1)
    const [filialConfiguracao, setfilialConfiguracao] = useState(null);

    async function load(cancelToken){
        setLoading(true);
        try{
            const provs = await Api.Filial.list_all(cancelToken)
            setFiliais(provs)
        } catch(error) {

        } finally{
            setLoading(false)
        }
    }

    const disableUser = (id) => async () => {
        try{
            await Api.Filial.disable(id);
            isFunction(refresh) && refresh()
        } catch(erro) {

        }
    }

    const enableUser = (id) => async () => {
        try{
            await Api.Filial.enable(id);
            isFunction(refresh) && refresh()
        } catch(erro) {

        }
    }

    const onCloseChange = React.useCallback(() => {
        setfilialConfiguracao(null)
        const cancelToken = Api.Auth.getCancelToken()
        load(cancelToken, 1)
        return () => cancelToken.cancel()
    }, [load])

    useEffect(() => {
        const cancelToken = Api.Auth.getCancelToken()
        load(cancelToken, pagina)
        return () => cancelToken.cancel()
    }, [pagina, shouldRefresh])

    return (
        <>
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Nome</Table.HeaderCell>
                        <Table.HeaderCell textAlign="right">Ativo?</Table.HeaderCell>
                        <Table.HeaderCell width={2} textAlign="right">Usuários da filial</Table.HeaderCell>
                        <Table.HeaderCell width={2} textAlign="right">Configurações</Table.HeaderCell>
                        <Table.HeaderCell width={1} textAlign="right">Ações</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    { Array.isArray(filiais) && filiais.map(filial => (
                        <Table.Row key={filial._id}>
                            <Table.Cell>{filial.nomeFilial}</Table.Cell>
                            <Table.Cell textAlign="right">
                                {filial.enabled && <><Icon name="check circle" color="green" />Sim</>}
                                {!filial.enabled && <><Icon name="times circle" color="red" />Não</>}
                            </Table.Cell>
                            <Table.Cell textAlign="right">
                                <Link to={USUARIOS_FILIAIS.replace(":id", filial._id)}>
                                    <Button icon="users" content="Usuários" color={MAIN_COLOR}  />
                                </Link>
                            </Table.Cell>
                            <Table.Cell textAlign="right">
                                <Button icon="cog" onClick={() => setfilialConfiguracao(filial)} content="Config." compact title="Configurações da filial" color={MAIN_COLOR}  />
                            </Table.Cell>
                            <Table.Cell textAlign="right">
                                { filial.nomeFilial !== 'MATRIZ' &&  <Dropdown icon="bars" >
                                    <Dropdown.Menu>
                                        { filial.enabled &&
                                            <Menu.Item icon="times" onClick={disableUser(filial._id)} content="Desativar" />
                                        }
                                        { !filial.enabled &&
                                            <Menu.Item icon="check" onClick={enableUser(filial._id)} content="Ativar" />
                                        }
                                    </Dropdown.Menu>
                                </Dropdown>}
                            </Table.Cell>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
            <ConfiguracoesFilial filial={filialConfiguracao} onClose={onCloseChange} />
            <Dimmer inverted active={loading}>
                <Loader>Carregando...</Loader>
            </Dimmer>
        </>
    )
}

export default ListaFiliais