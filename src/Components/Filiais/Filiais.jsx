import React, { useState } from 'react'
import { Segment, Header } from 'semantic-ui-react'
import ListaFiliais from './ListaFiliais'
import FormFilial from './FormFilial'

const Filiais = props => {
    const [refresh, setRefresh] = useState(1);

    return (
        <>
            <Header as="h2" attached="top">
                <Header.Content>
                    Cadastro de filiais
                </Header.Content>
            </Header>
            <Segment  attached padded>
                <FormFilial refresh={() => setRefresh(refresh+1)} />
            </Segment>

            <Header as="h2" attached="top">
                <Header.Content>
                    Filiais
                </Header.Content>
            </Header>
            <Segment attached padded>
                <ListaFiliais refresh={() => setRefresh(refresh+1)} shouldRefresh={refresh}/>
            </Segment>
        </>
    )
}
  
export default Filiais