import React, { useState, useEffect } from 'react'
import { Form, Message } from 'semantic-ui-react'
import Api from '../../Api'
import { MAIN_COLOR } from '../../environment'
import { isFunction } from 'util'

const ESTADO_INICIAL = {
    nomeFilial: '',
}

const FormFilial = props => {
    const { refresh } = props;

    const [loading, setLoading] = useState(false);
    const [form, setForm] = useState({...ESTADO_INICIAL})
    const [erro, setErro] = useState(false);
    const [sucesso, setSucesso] = useState(false);
    const [mensagem, setMensagem] = useState('');

    async function submit(){
        setLoading(true)
        setErro(false);
        setSucesso(false);
        setMensagem('');
        try{
            const dados = {...form}
            validate(dados)
            await Api.Filial.register(dados)
            setSucesso(true)
            setMensagem("Filial registrado com sucesso")
            isFunction(refresh) && refresh();
        }catch(erro){
            setErro(true)
            if(erro.response && erro.response.data){
                setMensagem(erro.response.data.erro)
            } else {
                setMensagem(erro.message)
            }
        } finally{
            setLoading(false);
        }
    }

    function validate(dados){
        if(!dados.nomeFilial){
            throw new Error("Nome é obrigatório")
        }
    }

    function changeForm(e, { name, value }) {
        setForm({ ...form, [name]: value })
    }

    return (
        <Form loading={loading} error={erro} success={sucesso} onSubmit={submit}>
            <Form.Field required>
                <label>Nome</label>
                <Form.Input required name="nomeFilial" onChange={changeForm} value={form.nomeFilial} />
            </Form.Field>
            <Message error icon="times circle" color="red" content={mensagem} />
            <Message success icon="check circle" color="green" content={mensagem} />
            <Form.Button loading={loading} color={MAIN_COLOR} fluid content="Cadastrar filial" icon="save" />
        </Form>
    )
}

export default FormFilial