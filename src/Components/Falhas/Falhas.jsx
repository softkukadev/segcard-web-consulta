import React, { useEffect, useState } from "react";
import Moment from "react-moment";
import { Button, Pagination, Segment, Table, Input, Icon } from "semantic-ui-react";
import Api from "../../Api";

const Falhas = (props) => {
  const [falhas, setFalhas] = useState([]);
  const [paginaAtual, setPaginaAtual] = useState(1);
  const [numPaginas, setNumPaginas] = useState(1);
  const [loading, setLoading] = useState(false);
  const [codigoLote, setCodigoLote] = useState('');

  async function load(cancelToken, pagina) {
    setLoading(true);
    try {
      const itens = await Api.Sell.failed(cancelToken, pagina, {codigoLote});
      setFalhas(itens.docs);
      setNumPaginas(itens.totalPages);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    const cancelToken = Api.Sell.getCancelToken();
    load(cancelToken, paginaAtual);
    return () => cancelToken.cancel();
  }, [paginaAtual]);

  const reloadSearch = () => {
    const cancelToken = Api.Sell.getCancelToken();
    load(cancelToken, paginaAtual);
  }
  const exportar = async () => {
    const cancelToken = Api.Sell.getCancelToken();
    const itens = await Api.Sell.failed(cancelToken, undefined, [], "csv");

    window.URL = window.webkitURL || window.URL;
    var csvFile = new Blob([itens], {
      type: "text/csv; charset=utf-8",
    });
    var link = document.createElement("a");
    link.setAttribute("href", window.URL.createObjectURL(csvFile));
    link.setAttribute("download", "falhas.csv");
    link.dataset.downloadurl = ["text/csv", link.download, link.href].join(":");
    document.body.appendChild(link);
    link.click();
    link.remove();
  };

  return (
    <Segment loading={loading}>
      <Input onChange={(e, t) => setCodigoLote(t.value)} label="Código do lote" placeholder="Código do lote" style={{width: '30%'}} value={codigoLote} icon={<Icon title="Pesquisar por codigo do lote" link onClick={reloadSearch} name="search"/>} />
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Lote</Table.HeaderCell>
            <Table.HeaderCell>Nome</Table.HeaderCell>
            <Table.HeaderCell>CPF</Table.HeaderCell>
            <Table.HeaderCell>Telefone</Table.HeaderCell>
            <Table.HeaderCell>Ultimos digitos cartão</Table.HeaderCell>
            <Table.HeaderCell>Erro</Table.HeaderCell>
            <Table.HeaderCell>Data/Hora</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {falhas.map((falha, i) => {
            return (
              <Table.Row key={i}>
                <Table.Cell>{falha.codigoLote}</Table.Cell>
                <Table.Cell>{falha.cliente.name}</Table.Cell>
                <Table.Cell>{falha.cliente.cpf}</Table.Cell>
                <Table.Cell>{falha.cliente.phone_number}</Table.Cell>
                <Table.Cell>{falha.cliente.card}</Table.Cell>
                <Table.Cell>
                  {falha.provider === "paylab" && (
                    <ul>
                      {falha.retorno.data &&
                        falha.retorno.data.Messages &&
                        falha.retorno.data.Messages.map((message, i) => {
                          return (
                            <li key={i}>
                              <em>{message.Code}</em>: {message.Message}
                            </li>
                          );
                        })}
                    </ul>
                  )}
                  {falha.provider === "cielo" && (
                    <ul>
                      <li>
                        <em>Código {falha.ReasonCode}</em>:{" "}
                        {falha.ReasonMessage} ({falha.message})
                      </li>
                    </ul>
                  )}
                  {falha.provider === "payu" && (
                    <ul>
                      <li>
                        <em>Código {falha.retorno?.data?.type}</em>:{" "}
                        {falha.retorno?.data?.description ?? falha.retorno?.error}
                      </li>
                    </ul>
                  )}
                  {falha.provider === "pagseguro" && (
                    <ul>
                      <li>
                        <em>Código {falha.retorno?.data?.payment_response?.code}</em>:{" "}
                        {falha?.retorno?.data?.payment_response?.message}
                      </li>
                    </ul>
                  )}
                  {falha.provider === "rede" && (
                    <ul>
                      <li>
                        <em>Código {falha.retorno?.response_error?.code}</em>:{" "}
                        {falha.retorno?.response_error?.message}
                      </li>
                    </ul>
                  )}
                  {falha.provider === "vindi" && (
                    <ul>
                      <li>
                        <em>Código {falha.retorno?.response_error?.code || falha.retorno?.data?.errors?.reduce((acc, item) => acc + item?.parameter + ' - '+item?.message, '')}</em>:{" "}
                        {falha.retorno?.response_error?.message}
                      </li>
                    </ul>
                  )}
                  {falha.provider === "sk-payu" && (
                    <ul>
                      <li>
                        <em>
                          Código{" "}
                          {
                            falha.retorno?.transactionResponse
                              ?.paymentNetworkResponseCode
                          }
                        </em>
                        : {falha.retorno?.transactionResponse?.responseCode ?? falha.retorno?.error} -
                        {
                          falha.retorno?.transactionResponse
                            ?.paymentNetworkResponseErrorMessage ?? falha.retorno?.transactionResponse
                            ?.responseMessage
                        }
                      </li>
                    </ul>
                  )}
                  {falha.provider === "hubsale" && (
                    <ul>
                      <li>
                        <em>
                          Código{" "}
                          {
                            falha.retorno?.response_error?.code
                          }
                        </em>
                        : {falha.retorno?.response_error?.message}
                      </li>
                    </ul>
                  )}
                </Table.Cell>
                <Table.Cell>
                  {falha.created_at && (
                    <Moment format="DD/MM/YYYY HH:mm:ss">
                      {falha.created_at}
                    </Moment>
                  )}
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
      <Pagination
        totalPages={numPaginas}
        defaultActivePage={1}
        onPageChange={(e, t) => setPaginaAtual(t.activePage)}
      />
      <Button
        floated="right"
        content="Exportar para CSV"
        onClick={exportar}
        color="green"
        icon="table"
      />
    </Segment>
  );
};

export default Falhas;
