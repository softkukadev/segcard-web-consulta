import React, { useState, useEffect } from 'react';
import moment from 'moment'
import Api from '../../Api';
import { Table, Button, Loader, Dimmer, Segment, Label, Grid, Header, Form, List, Divider, Select } from 'semantic-ui-react';
import 'moment/locale/pt-br'
import { useMemo } from 'react';
import { ResponsiveContainer, LineChart, XAxis, CartesianGrid, YAxis, Tooltip, Line } from 'recharts';
import ModalPorFilial from './ModalPorFilial'
import { useCallback } from 'react';

const ESTADO_INICIAL_DIALOG = {
    open: false,
    data: '',
    campo: '',
    label: ''
}

const TabelaMesAno = props => {
    const { currentRole } = props
    const [loading, setLoading] = useState(false);
    const [dadosTabela, setDadosTabela] = useState([]);
    const [dadosTabelaFilial, setDadosTabelaFilial] = useState([]);
    const [filiais, setFiliais] = useState([]);

    const [mesReferencia, setMesReferencia] = useState(moment().month());

    const [dadosDialog, setDadosDialog] = useState({ ...ESTADO_INICIAL_DIALOG })

    useEffect(() => {
        async function load(cancelToken) {
            setLoading(true);
            try {
                const filiais = await Api.Filial.list_all(cancelToken);
                setFiliais(filiais);
                
                const data = await Api.Sell.estatisticas_dia_mes(cancelToken);
                setDadosTabela(data);

                const dataFilial = await Api.Sell.estatisticas_dia_mes(cancelToken, true);
                setDadosTabelaFilial(dataFilial);
            } catch (error) {
                setDadosTabela([]);
            } finally {
              setLoading(false);
            }
          }
          const cancel = Api.Sell.getCancelToken();
          load(cancel);
          return () => cancel.cancel("");
    }, [])

    const onClose = useCallback(() => {
        setDadosDialog({...ESTADO_INICIAL_DIALOG})
    }, [])

    const openDialog = useCallback((data, campo, label) => {
        setDadosDialog({
            open: true,
            campo,
            label,
            data
        })
    }, [])

    const meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
    const ano = moment().year()
    
    const mesesOptions = useMemo(() => {
        return meses.map((mes, i) => ({ key: mes, value: i, text: mes }))
    }, [meses])
    
    const mesIteracaoTabela = useMemo(() => moment().set('month', mesReferencia).startOf('month'), [mesReferencia]);
    
    const numDiasMes = mesIteracaoTabela.daysInMonth();
    const dias = Array(numDiasMes).fill(0).map((c,i) => i + 1);
    
    const m = (mesReferencia + 1).toString().padStart(2, '0');

    const apenasDiasMesReferencia = useMemo(() => {
        return dadosTabela.filter(d => moment(d.data, 'DD/MM/YYYY').month() === mesReferencia);
    }, [dadosTabela, mesReferencia])

    return <Segment loading={loading}>
        <Header as="h3">Selecione o mês de referência</Header>
        <Select options={mesesOptions} value={mesReferencia} onChange={(e,{value}) => setMesReferencia(value)} fluid labeled  />
        <Table celled="internally" compact definition fixed striped selectable structured >
            <Table.Header>
                <Table.HeaderCell style={{width: '7%'}}>Tipo/Dia</Table.HeaderCell>
                {dias.map(dia => <Table.HeaderCell textAlign="center" key={dia}>{dia}</Table.HeaderCell>)}
            </Table.Header>

            <Table.Row>
                <Table.Cell>Esperado Recorrência</Table.Cell>
                {dias.map(dia => {
                    const d = dia.toString().padStart(2, '0');
                    const formatado = `${d}/${m}/${ano}`;

                    const dados = dadosTabela.find(c => c.data === formatado);
                    let esperado = 0;
                    let efetivado = 0;
                    if(dados) {
                        esperado = dados.esperado;
                        efetivado = dados.efetivado;
                    }

                    return <Table.Cell  key={`${mesReferencia}-${dia}`} textAlign="center">
                        <span style={{color: '#05a139', cursor: 'pointer'}} onClick={() =>  openDialog(formatado, 'esperado', 'Esperado recorrência')} title={esperado}>{esperado}</span><br/>
                    </Table.Cell>
                })}
            </Table.Row>

            <Table.Row>
                <Table.Cell>Efetivado Recorrência</Table.Cell>
                {dias.map(dia => {
                    const d = dia.toString().padStart(2, '0');
                    const formatado = `${d}/${m}/${ano}`;

                    const dados = dadosTabela.find(c => c.data === formatado);

                    return <Table.Cell key={`${mesReferencia}-${dia}`} textAlign="center">
                        <span style={{fontWeight:'bold', color: '#05a33c', cursor: 'pointer'}} onClick={() =>  openDialog(formatado, 'recorrencias', 'Efetivado recorrência')} title={dados?.recorrencias || 0}>{dados?.recorrencias || 0}</span><br/>
                    </Table.Cell>
                })}
            </Table.Row>

            <Table.Row>
                <Table.Cell>Novas vendas</Table.Cell>
                {dias.map(dia => {
                    const d = dia.toString().padStart(2, '0');
                    const formatado = `${d}/${m}/${ano}`;

                    const dados = dadosTabela.find(c => c.data === formatado);

                    return <Table.Cell key={`${mesReferencia}-${dia}`} textAlign="center">
                        <span style={{fontWeight:'bold', color: '#05a33c', cursor: 'pointer'}} onClick={() =>  openDialog(formatado, 'novas', 'Novas vendas')} title={dados?.novas}>{dados?.novas || 0}</span><br/>
                    </Table.Cell>
                })}
            </Table.Row>

            <Table.Row>
                <Table.Cell>Novos + recorrência</Table.Cell>
                {dias.map(dia => {
                    const d = dia.toString().padStart(2, '0');
                    const formatado = `${d}/${m}/${ano}`;

                    const dados = dadosTabela.find(c => c.data === formatado);
                    let repique_sucessos = 0;
                    let efetivado = 0;
                    if(dados) {
                        repique_sucessos = dados.repique_sucessos;
                        efetivado = dados.efetivado;
                    }

                    return <Table.Cell key={`${mesReferencia}-${dia}`} textAlign="center">
                        <span style={{fontWeight:'bold', color: '#05a33c', cursor: 'pointer'}} onClick={() =>  openDialog(formatado, 'efetivado', 'Novos + recorrências')}  title={efetivado - repique_sucessos}>{efetivado - repique_sucessos}</span><br/>
                    </Table.Cell>
                })}
            </Table.Row>

            
            <Table.Row>
                <Table.Cell>Sucesso repiques</Table.Cell>
                {dias.map(dia => {
                    const d = dia.toString().padStart(2, '0');
                    const formatado = `${d}/${m}/${ano}`;

                    const dados = dadosTabela.find(c => c.data === formatado);
                    let repique_sucessos = 0;
                    if(dados) {
                        repique_sucessos = dados.repique_sucessos;
                    }

                    return <Table.Cell key={`${mesReferencia}-${dia}`} textAlign="center">
                        <span style={{fontWeight:'bold', color: '#05a33c', cursor: 'pointer'}} onClick={() =>  openDialog(formatado, 'repique_sucessos', 'Repiques: Sucesso')} title={repique_sucessos}>{repique_sucessos}</span><br/>
                    </Table.Cell>
                })}
            </Table.Row>

            <Table.Row>
                <Table.Cell>Total sucessos</Table.Cell>
                {dias.map(dia => {
                    const d = dia.toString().padStart(2, '0');
                    const formatado = `${d}/${m}/${ano}`;

                    const dados = dadosTabela.find(c => c.data === formatado);
                    let esperado = 0;
                    let efetivado = 0;
                    if(dados) {
                        esperado = dados.esperado;
                        efetivado = dados.efetivado;
                    }

                    return <Table.Cell key={`${mesReferencia}-${dia}`} textAlign="center">
                        <span style={{fontWeight:'bold', color: '#05a33c', cursor: 'pointer'}} onClick={() =>  openDialog(formatado, 'efetivado', 'Efetivado total')} title={efetivado}>{efetivado}</span><br/>
                    </Table.Cell>
                })}
            </Table.Row>
            
            <Table.Row>
                <Table.Cell>Falhas de recorrência</Table.Cell>
                {dias.map(dia => {
                    const d = dia.toString().padStart(2, '0');
                    const formatado = `${d}/${m}/${ano}`;

                    const dados = dadosTabela.find(c => c.data === formatado);
                    let repique_falhas = 0;
                    let falhas = 0;
                    if(dados) {
                        repique_falhas = dados.repique_falhas;
                        falhas = dados.falhas;
                    }

                    return <Table.Cell key={`${mesReferencia}-${dia}`} textAlign="center">
                        <span style={{fontWeight:'bold', color: '#b52a2a'}} title={falhas - repique_falhas}>{falhas - repique_falhas}</span><br/>
                    </Table.Cell>
                })}
            </Table.Row>

            <Table.Row>
                <Table.Cell>Falhas repiques</Table.Cell>
                {dias.map(dia => {
                    const d = dia.toString().padStart(2, '0');
                    const formatado = `${d}/${m}/${ano}`;

                    const dados = dadosTabela.find(c => c.data === formatado);
                    let repique_falhas = 0;
                    if(dados) {
                        repique_falhas = dados.repique_falhas;
                    }

                    return <Table.Cell key={`${mesReferencia}-${dia}`} textAlign="center">
                        <span style={{fontWeight:'bold', color: '#b52a2a', cursor: 'pointer'}} onClick={() =>  openDialog(formatado, 'repique_falhas', 'Repique: FALHAS')} title={repique_falhas}>{repique_falhas}</span><br/>
                    </Table.Cell>
                })}
            </Table.Row>


            <Table.Row>
                <Table.Cell>Total falhas</Table.Cell>
                {dias.map(dia => {
                    const d = dia.toString().padStart(2, '0');
                    const formatado = `${d}/${m}/${ano}`;

                    const dados = dadosTabela.find(c => c.data === formatado);
                    let falhas = 0;
                    if(dados) {
                        falhas = dados.falhas;
                    }

                    return <Table.Cell key={`${mesReferencia}-${dia}`} textAlign="center">
                        <span style={{fontWeight:'bold', color: '#b52a2a', cursor: 'pointer'}} onClick={() =>  openDialog(formatado, 'falhas', 'Falhas')} title={falhas}>{falhas}</span><br/>
                    </Table.Cell>
                })}
            </Table.Row>


        </Table>       

        <ResponsiveContainer width={'100%'} height={400} >
            <LineChart data={apenasDiasMesReferencia}
                margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="data" />
                <YAxis/>
                <Tooltip />
                <Line type="monotone" strokeWidth={3} stroke="#05a33c" dataKey="efetivado"  />
                <Line type="monotone" strokeWidth={1} strokeDasharray stoke="#95a5a6" dataKey="esperado"  />
                <Line type="monotone" strokeWidth={2} stoke="#56d0d6" dataKey="repiques_sucessos"  />
                <Line type="monotone" strokeWidth={2} color="#c9981c"  dataKey="repiques_falhas"  />
                <Line type="monotone" strokeWidth={2} color="#b81631"  dataKey="falhas"  />
            </LineChart>
        </ResponsiveContainer>     
        <ModalPorFilial dados={dadosTabelaFilial} onClose={onClose} filiais={filiais} {...dadosDialog} />
    </Segment>
}

export default TabelaMesAno;