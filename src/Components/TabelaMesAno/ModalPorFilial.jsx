import React, { useState, useEffect, useMemo } from 'react'
import { Modal, Table, Button, Label, Dimmer, Loader, Pagination, Header } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import moment from 'moment'
import Api from '../../Api'
import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import { VISUALIZAR_ASSINATURA } from '../../Navigator/routes';
import { MAIN_COLOR } from '../../environment';

const ModalPorFilial = props => {
    const { dados, filiais, campo, label, data, open, onClose } = props;

    const dadosFiliais = useMemo(() => {
        if(!open) {
            return []
        }
        const ret = []
        for (let filial of filiais) {
            const {nomeFilial, _id} = filial;
            const dadosFilial = dados.find(c => c.idFilial === _id)
            if(dadosFilial){
                const { dados: df } = dadosFilial;
                const dadosDia = df.find(c => c.data === data);
                ret.push({ nome: nomeFilial, qtd: dadosDia[campo] });
            }
        }
        return ret
    }, [dados, filiais, campo, data, open])

    if(!open) {
        return null
    }

    return <Modal size="large" centered={false} open={open} closeIcon onClose={onClose}>
        <Modal.Header>{data}</Modal.Header>
        <Modal.Content>
            <Modal.Description>
                <Header as="h2" content={label} />
                <Table>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Filial</Table.HeaderCell>
                            <Table.HeaderCell>Quantidade</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {dadosFiliais.map(dado => (
                            <Table.Row key={dado.nome}>
                                <Table.Cell>{dado.nome}</Table.Cell>
                                <Table.Cell>{dado.qtd}</Table.Cell>
                            </Table.Row>
                        ))}
                    </Table.Body>
                </Table>
            </Modal.Description>
        </Modal.Content>
    </Modal>
}

ModalPorFilial.propTypes = {
    dados: PropTypes.array,
    filiais: PropTypes.array,
    campo: PropTypes.string,
    label: PropTypes.string,
    data: PropTypes.string,
    open: PropTypes.bool,
    onClose: PropTypes.func.isRequired
}

export default ModalPorFilial;