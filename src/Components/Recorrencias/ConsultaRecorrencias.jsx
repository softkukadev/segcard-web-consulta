import React, { useState, useEffect, useMemo, useContext } from 'react';
import moment from 'moment'
import { Calendar, momentLocalizer } from 'react-big-calendar'
import '../../../node_modules/react-big-calendar/lib/css/react-big-calendar.css';
import Api from '../../Api';
import { Table, Button, Loader, Dimmer, Segment, Label, Pagination, Header } from 'semantic-ui-react';
import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import { VISUALIZAR_ASSINATURA } from '../../Navigator/routes';
import { MAIN_COLOR } from '../../environment';
import FiltrosVendas from '../ListaVendas/FiltrosVendas';
import ModalVisualizarPagamentosTipo from './ModalVisualizarPagamentosTipo';
import FiltroDatas from '../FiltroDatas';
import FilialContext from '../../Helpers/FilialContext';
import {
    useJsonToCsv
} from 'react-json-csv';

const localizer = momentLocalizer(moment)

const fields = {
    "created_at": "data",
    "id_transacao": "ID Transação",
    "id_assinatura": "ID Assinatura",
    "status": "Status",
    "cpf": "cpf",
    "name": "Nome",
    "idFilial": "idFilial",
    "operador": "Nome Operador"
}

const percentualTamper = 0.3;

const ConsultaRecorrencias = props => {
    const { currentRole } = props;
    
    const { saveAsCsv } = useJsonToCsv();
    const filialContext = useContext(FilialContext)

    const [totalRegistros, setTotalRegistros] = useState(0);
    const [eventoAberto, setEventoAberto] = useState(null);    
    const [periodo, setPeriodo] = useState({
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month'),
        option: 7
    })

    const [pagamentos, setPagamentos] = useState([])
    const [pagamentosCalendario, setPagamentosCalendario] = useState([])
    const [loading, setLoading] = useState(false)

    const [dataInicio, setDataInicio] = useState(moment().startOf('month'))
    const [dataFim, setDataFim] = useState(moment().endOf('month'))

    const [filtros, setFiltros] = useState({ datas:'' })
    const [page, setPage] = useState(1);
    
    const numPaginas = useMemo(() => {
        if(pagamentos) {
            return Math.ceil(pagamentos.length / 50);
        }
    },[pagamentos])

    const pagamentosPagina = useMemo(() => {
        if(pagamentos) {
            return pagamentos.slice((page - 1) * 50, page * 50)
        }
        return []
    }, [page, pagamentos])

    const fnOpenEvento = (evento) => setEventoAberto(evento);
    const fnCloseEvento = () => setEventoAberto(null);
    

    const generateCsv = () => { 
        const data = pagamentos.map(pagto => ({
            created_at: pagto.created_at,
            id_transacao: pagto.id_transacao,
            id_assinatura: pagto.id_assinatura,
            status: pagto.status,
            name: pagto.assinatura?.name,
            idFilial: pagto.idFilial,
            cpf: pagto.assinatura?.cpf,
            operador: pagto.assinatura?.operador?.name
        }))
        saveAsCsv({ data, fields, filename: 'exportacao-pagamentos', separator: ';' })
    }

    async function loadPagamentos(cancelToken, dataInicio, dataFim, filtros) {
        setLoading(true);
        try {
          const data = await Api.Sell.pagamentos_por_data(dataInicio?.startOf('day')?.utc(), dataFim.endOf('day'), filtros, cancelToken);
          setPagamentos(data);

          const calendario = await Api.Sell.pagamentos_calendario(dataInicio, dataFim, cancelToken);
          setPagamentosCalendario(calendario);

          if(!filtros.status) {
              setTotalRegistros(data.length);
          }
        } catch (error) {
          setPagamentos([]);
          setPagamentosCalendario([]);
        } finally {
          setLoading(false);
        }
    }

    const callbackDatas = (dataInicio, dataFim) => {
        const cancel = Api.Sell.getCancelToken();
        setDataInicio(dataInicio)
        setDataFim(dataFim)
        loadPagamentos(cancel, dataInicio, dataFim, filtros);
    }

    useEffect(() => {
        const cancel = Api.Sell.getCancelToken();
        loadPagamentos(cancel, dataInicio, dataFim, filtros);
        return () => cancel.cancel("");
      }, [dataInicio, dataFim, filtros]);


    const calendarEvents = React.useMemo(() => {
        if(Array.isArray(pagamentosCalendario)) {
            return pagamentosCalendario.map(dia => {
                const { _id: { data, status, tipo }, total } = dia;

                return {
                    title: `${total} ${status === 'created' ? 'Sucessos' : 'Falhas'} ${tipo === 'repique' ? 'repique': ''}`,
                    start: moment(data, 'DD/MM/YYYY').add('3', 'hours').toDate(),
                    end: moment(data, 'DD/MM/YYYY').add('3', 'hours').toDate(),
                    allDay: false,
                    resource: {
                        tipo,
                        status,
                        total
                    }
                }
            })
        }
        return []
    }, [ pagamentosCalendario])

    const totalTamper = React.useMemo(() => {
        if(filialContext.isFilial) {
            if(filtros.status === 'created') {
                return Math.ceil((percentualTamper * pagamentos.length)) * -1;
            } else if(filtros.status === 'FAILED'){
                return Math.ceil((totalRegistros - pagamentos.length) * percentualTamper); 
            } else {
                return 0;
            }
        } else {
            return 0;
        }
}, [filialContext, filtros, pagamentos])

    return <Segment>
        {!filialContext.isFilial && <Calendar
        defaultView="month"
        views={['month']}
        dayLayoutAlgorithm="no-overlap"
        localizer={localizer}
        events={calendarEvents}
        onSelectEvent={fnOpenEvento}
        startAccessor="start"
        onRangeChange={({start, end}) =>{ 
            setDataInicio(moment(start))
            setDataFim(moment(end))
        }}
        selectable
        endAccessor="end"
        style={{ height: 950 }}
        />}

        <FiltrosVendas applyFilters={setFiltros} tipo="pagamentos" currentRole={currentRole} />
        <FiltroDatas periodo={periodo} setPeriodo={setPeriodo} callback={callbackDatas} />
        <Header content={`${pagamentos.length + totalTamper} registros`} as="h2" floated="right" />
        {!filialContext.isFilial && <Button floated="right" onClick={generateCsv} icon="table" content="Exportar" ></Button>}
        <Table>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Data</Table.HeaderCell>
                    <Table.HeaderCell>CPF</Table.HeaderCell>
                    <Table.HeaderCell>Nome</Table.HeaderCell>
                    <Table.HeaderCell>Cartao</Table.HeaderCell>
                    <Table.HeaderCell>ID transação</Table.HeaderCell>
                    <Table.HeaderCell>Status</Table.HeaderCell>
                    <Table.HeaderCell>Operador</Table.HeaderCell>
                    <Table.HeaderCell>Ações</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {
                    pagamentosPagina.map(pagamento => (
                        <Table.Row key={pagamento._id}>
                            <Table.Cell>
                                <Moment format="DD/MM/YYYY HH:mm:ss">{pagamento.created_at}</Moment>
                            </Table.Cell>
                            <Table.Cell>
                                {pagamento.assinatura.cpf}
                            </Table.Cell>
                            <Table.Cell>
                                {pagamento.assinatura.name}
                            </Table.Cell>
                            <Table.Cell>
                                {pagamento.assinatura.card}
                            </Table.Cell>

                            <Table.Cell style={{fontSize: 11}}>
                                {pagamento.id_transacao}
                            </Table.Cell>
                            <Table.Cell style={{width: '15%'}}>
                                {pagamento.repique && pagamento?.num_repiques > 0 ?
                                    <Label circular size="small" horizontal color="orange" content={`Repique: ${pagamento.num_repiques}`} />
                                    :
                                    <Label circular size="small" horizontal color="green" content={`Normal`} />
                                }
                                {pagamento.status?.toUpperCase()}
                            </Table.Cell>
                            <Table.Cell>
                                {pagamento.assinatura.operador.name}
                            </Table.Cell>
                            <Table.Cell>
                                <Link to={VISUALIZAR_ASSINATURA.replace(':id', pagamento.id_assinatura)}>
                                    <Button icon="arrow right" labelPosition="right" color={MAIN_COLOR} compact size="small" content="Ir para assinatura" />
                                </Link>
                            </Table.Cell>
                        </Table.Row>
                    ))
                }
            </Table.Body>
        </Table>
        <Pagination activePage={page} onPageChange={(e,t) => setPage(t.activePage)} totalPages={numPaginas} />
        <ModalVisualizarPagamentosTipo evento={eventoAberto} onClose={fnCloseEvento} />
        <Dimmer inverted  active={loading}>
            <Loader >Carregando</Loader>
        </Dimmer>
    </Segment>
}

export default ConsultaRecorrencias;