import React, { useState, useEffect, useMemo } from 'react'
import { Modal, Table, Button, Label, Dimmer, Loader, Pagination } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import moment from 'moment'
import Api from '../../Api'
import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import { VISUALIZAR_ASSINATURA } from '../../Navigator/routes';
import { MAIN_COLOR } from '../../environment';
const ModalVisualizarPagamentosTipo = props => {
    const { evento, onClose } = props;
    const [pagamentos, setPagamentos] = useState([])
    const [loading, setLoading] = useState(false);

    const [page, setPage] = useState(1);

    const numPaginas = useMemo(() => {
        if(pagamentos) {
            return Math.ceil(pagamentos.length / 50);
        }
    },[pagamentos])

    const pagamentosPagina = useMemo(() => {
        if(pagamentos) {
            return pagamentos.slice((page - 1) * 15, page * 15)
        }
        return []
    }, [page, pagamentos])

    useEffect(() => {
        const cancelToken = Api.Sell.getCancelToken();
        if(evento) {
            async function load() {
                setLoading(true)
                try {
                    const data = await Api.Sell.pagamentos_por_data(moment(evento.start).startOf('day'), moment(evento.end).endOf('day'), { tipo: evento?.resource?.tipo, status: evento?.resource?.status }, cancelToken);
                    setPagamentos(data);
                } finally{
                    setLoading(false)
                }
            }
            load()
        }
        return () => cancelToken.cancel()
    }, [evento])

    if(!evento) {
        return null
    }

    return <Modal size="large" centered={false} open={evento !== null} closeIcon onClose={onClose}>
        <Modal.Header>{moment(evento.start).format("DD/MM/YYYY")} - {evento?.resource?.status === 'created'? 'SUCESSO' : 'FALHA'}: {evento?.resource?.tipo.toUpperCase()}</Modal.Header>
        <Modal.Content>
        <Modal.Description>
        <Table>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Data</Table.HeaderCell>
                    <Table.HeaderCell>CPF</Table.HeaderCell>
                    <Table.HeaderCell>Nome</Table.HeaderCell>
                    <Table.HeaderCell>Cartao</Table.HeaderCell>
                    <Table.HeaderCell>ID transação</Table.HeaderCell>
                    <Table.HeaderCell>Status</Table.HeaderCell>
                    <Table.HeaderCell>Operador</Table.HeaderCell>
                    <Table.HeaderCell>Ações</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {
                    pagamentosPagina.map(pagamento => (
                        <Table.Row key={pagamento._id}>
                            <Table.Cell>
                                <Moment format="DD/MM/YYYY HH:mm:ss">{pagamento.created_at}</Moment>
                            </Table.Cell>
                            <Table.Cell>
                                {pagamento.assinatura.cpf}
                            </Table.Cell>
                            <Table.Cell>
                                {pagamento.assinatura.name}
                            </Table.Cell>
                            <Table.Cell>
                                {pagamento.assinatura.card}
                            </Table.Cell>

                            <Table.Cell style={{fontSize: 11}}>
                                {pagamento.id_transacao}
                            </Table.Cell>
                            <Table.Cell style={{width: '15%'}}>
                                {pagamento.repique && pagamento?.num_repiques > 0 ?
                                    <Label circular size="small" horizontal color="orange" content={`Repique: ${pagamento.num_repiques}`} />
                                    :
                                    <Label circular size="small" horizontal color="green" content={`Normal`} />
                                }
                                {pagamento.status?.toUpperCase()}
                            </Table.Cell>
                            <Table.Cell>
                                {pagamento.assinatura.operador.name}
                            </Table.Cell>
                            <Table.Cell>
                                <Link to={VISUALIZAR_ASSINATURA.replace(':id', pagamento.id_assinatura)}>
                                    <Button icon="arrow right" labelPosition="right" color={MAIN_COLOR} compact size="small" content="Ir para assinatura" />
                                </Link>
                            </Table.Cell>
                        </Table.Row>
                    ))
                }
            </Table.Body>
        </Table>
        <Pagination activePage={page} onPageChange={(e,t) => setPage(t.activePage)} totalPages={numPaginas} />
        </Modal.Description>
        <Dimmer active={loading} inverted>
                <Loader>Carregando</Loader>
        </Dimmer>
        </Modal.Content>
    </Modal>
}

ModalVisualizarPagamentosTipo.propTypes = {
    evento: PropTypes.object,
    onClose: PropTypes.func.isRequired
}

export default ModalVisualizarPagamentosTipo;