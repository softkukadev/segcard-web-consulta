import React, { useState, useEffect } from 'react';
import moment from 'moment'
import Api from '../../Api';
import { Table, Button, Loader, Dimmer, Segment, Label, Grid, Header, Form, List, Divider } from 'semantic-ui-react';
import {
    DatesRangeInput
  } from 'semantic-ui-calendar-react';
import { ResponsiveContainer, PieChart, Pie, Legend, Cell, Tooltip, YAxis, XAxis, CartesianGrid, LineChart, Line } from 'recharts';
import 'moment/locale/pt-br'
import { useMemo, useContext } from 'react';
import FilialContext from '../../Helpers/FilialContext';


const percentualTamper = 0.3;
const ControleMensal = props => {
    const { currentRole } = props;

    const [periodo, setPeriodo] = useState(`${moment().format('DD/MM/YYYY')} - ${moment().format('DD/MM/YYYY')}`);
    const [registrosAgrupados, setRegistrosAgrupados] = useState([])
    const [registros, setRegistros] = useState([]);
    const [loading, setLoading] = useState(false);
    const [valorPlano, setValorPlano] = useState(0);

    const filialContext = useContext(FilialContext);

    const { dataInicio, dataFim } = useMemo(() => {
        const [ini, fim] = periodo.split(' - ');
        if(ini.trim() && fim.trim()){
            const  dataInicio = moment(ini, 'DD/MM/YYYY').startOf('day').toDate()
            const  dataFim = moment(fim, 'DD/MM/YYYY').endOf('day').toDate()
            return { dataInicio, dataFim }
        }
        return { dataInicio: moment().startOf('day').toDate(), dataFim: moment().endOf('day').toDate()}
    }, [periodo])

    useEffect(() => {
        async function loadPagamentos(cancelToken, dataInicio, dataFim) {
            setLoading(true);
            try {
                const calendario = await Api.Sell.pagamentos_calendario(dataInicio, dataFim, cancelToken);
                setRegistrosAgrupados(calendario);

                
                const data = await Api.Sell.pagamentos_por_data(dataInicio, dataFim, {}, cancelToken);
                setRegistros(data);

                const {value} = await Api.Provider.getParamentro('VALOR_PLANO');
                
                setValorPlano(+value);

            } catch (error) {
                setRegistrosAgrupados([]);
                setRegistros([])
            } finally {
                setLoading(false);
            }
        }
        const cancel = Api.Sell.getCancelToken();
        loadPagamentos(cancel, dataInicio, dataFim);
        return () => cancel.cancel("");
    }, [dataInicio, dataFim]);

    const totalPagamentos = useMemo(() => {
        if(Array.isArray(registrosAgrupados)) {
            return registrosAgrupados.reduce((acc, pag) => acc+pag.total,0)
        }
        return 0
    }, [registrosAgrupados])

    const pagamentosSucesso = useMemo(() => {
        if(Array.isArray(registrosAgrupados)) {
            const estados_sucesso = ['created', 'success'];
            return registrosAgrupados.filter(pag => estados_sucesso.includes(pag?._id?.status)).reduce((acc, pag) => acc+pag.total,0)
        }
        return 0
    }, [registrosAgrupados])

    const pagamentosFalhas = useMemo(() => {
        if(Array.isArray(registrosAgrupados)) {
            return registrosAgrupados.filter(pag => pag?._id?.status === 'FAILED').reduce((acc, pag) => acc+pag.total,0)
        }
        return 0
    }, [registrosAgrupados])

    const aprovadosChart = [
        { tipo: 'Aprovadas', total: pagamentosSucesso - (filialContext.isFilial ? Math.ceil(pagamentosSucesso * percentualTamper) : 0), color: '#0ec966' },
        { tipo: 'Falhas', total: pagamentosFalhas + (filialContext.isFilial ? Math.ceil(pagamentosSucesso * percentualTamper) : 0), color: '#c90e0e' },
    ]

    const agrupadosHora = useMemo(() => {
        const map_hour = registros.map(r => ({...r, hour: moment(r.created_at).hour()}))
        
        const arr = Array(24).fill(0);
        const registros_hora = map_hour.reduce((acc, item) => {
            acc[item.hour] += 1
            return acc         
        }, [...arr])

        const registros_grafico = registros_hora.map((total, i) => {
            return { hora: `${i.toString().padStart(2, '0')}:00`, total }
        })
        
        return registros_grafico
        
    }, [registros])

    const ErrosAgrupados = useMemo(() => {
        const codigos = registros.reduce((acc, item) => {
            if(item?.error){
                if(acc[item.error]){
                    acc[item.error] += 1
                } else {
                    acc[item.error] = 1;
                }
            }
            return acc
        }, {})
        return Object.keys(codigos).map(erro => {
            return { codigoErro: erro, total: codigos[erro] }
        }).sort((a,b) => b.total-a.total)
    }, [registros])

console.log(ErrosAgrupados);    


    return <Segment loading={loading}>
        <Form>
            <Form.Field>
                <label>Período</label>
                <DatesRangeInput localization="pt-BR" allowSameEndDate clearable dateFormat="DD/MM/YYYY" closable  value={periodo} onChange={(_, { value}) => setPeriodo(value)}></DatesRangeInput>
            </Form.Field>
        </Form>
        <Grid padded centered stackable  style={{marginTop: '2em'}} stretched   >
            <Grid.Column  width={8}>
                <Segment raised>
                    <Label color="green" size="large" ribbon>Pagamentos</Label>
                    <ResponsiveContainer width={'100%'} height={300} >
                        <PieChart>
                            <Pie data={aprovadosChart} cx="50%" cy="50%" dataKey="total" nameKey="tipo" outerRadius={90} innerRadius={60} fill="#8884d8" label paddingAngle={2}>
                                {aprovadosChart.map(cell => <Cell fill={cell.color} />)}
                            </Pie>
                            <Legend formatter={(v, entry, i) => v ? `${v} - ${((aprovadosChart[i]?.total / (totalPagamentos)) * 100)?.toFixed(2)}%` : ''} />
                            <Tooltip/>
                            <Tooltip/>
                        </PieChart>
                    </ResponsiveContainer>
                </Segment>
            </Grid.Column>
            <Grid.Column width={8}>
                <Segment raised>
                    <Label color="orange" size="large" ribbon>Valores</Label>
                    <Header as="h1" color="green">Total Aprovado: {Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(valorPlano * (pagamentosSucesso - (filialContext.isFilial ? Math.ceil(pagamentosSucesso * percentualTamper) : 0)))}</Header>
                    <Header as="h1" color="red">Total Falhas: {Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(valorPlano * (pagamentosFalhas + (filialContext.isFilial ? Math.ceil(pagamentosSucesso * percentualTamper) : 0)))}</Header>
                    <Header as="h1" color="blue">Total transacionado: {Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(valorPlano * (totalPagamentos))}</Header>
                </Segment>
            </Grid.Column>
        </Grid>
        <Grid stretched padded stackable style={{marginTop: '2em'}}>
            <Grid.Column width={10}>
                <Segment raised>
                    <Label size="large"  ribbon color={"black"}>Por horário</Label>
                    <ResponsiveContainer width={'100%'} height={400} >
                    <LineChart data={agrupadosHora}
                        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="hora" />
                        <YAxis />
                        <Tooltip />
                        <Line color="#1b83bf" type="monotone" strokeWidth={3}  dataKey="total" stroke="#1b83bf" />
                    </LineChart>
                    </ResponsiveContainer>
                </Segment>
            </Grid.Column>
            <Grid.Column width={6}>
                <Segment raised>
                    <Label size="large"  ribbon color={"red"}>Códigos de erro</Label>
                    <List bulleted size="large" celled divided>
                        {ErrosAgrupados.map(erro => <List.Item>
                            <List.Content>
                                {erro.codigoErro}
                                <span style={{float: 'right'}}>{erro.total}</span>
                            </List.Content>
                        </List.Item>)}
                    </List>
                </Segment>
            </Grid.Column>
        </Grid>
        <Divider/>
        
    </Segment>
}

export default ControleMensal;