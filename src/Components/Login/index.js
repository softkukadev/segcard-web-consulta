import React, { useState, useContext, useEffect } from "react";
import { withRouter } from "react-router-dom";
import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment,
} from "semantic-ui-react";
import { MAIN_COLOR } from "../../environment";
import routes from "../../Navigator/routes";
import Api from "./../../Api";
import FilialContext from "../../Helpers/FilialContext";

const LoginForm = ({ history }) => {
  const [user, setUser] = useState({ username: "", password: "", filial: '' });
  const [loading, setLoading] = useState(false);
  const [erro, setErro] = useState(null);
  const [filiais, setFiliais] = useState([]);
  
  const filialContext = useContext(FilialContext)

  useEffect(() => {
    async function getFiliais(cancel) {
      try {
        const resp = await Api.Filial.list_all(cancel);
        setFiliais(resp);
      } catch(error) {
        setFiliais([])
      }
    }
    const cancelToken = Api.Filial.getCancelToken()
    getFiliais(cancelToken)
    return () => {
      cancelToken.cancel()
    }
  }, [])


  const signin = () => {
    const authenticate = async () => {
      setLoading(true);
      try {
        const response = await Api.Auth.login(user.username, user.password, user.filial);
        if (response) {
          history.push(routes.INICIO);
        }
      } catch (error) {
        setErro(true);
        setLoading(false);
      }
    };
    authenticate();
  };

  const optionsFilial = React.useMemo(() => {
    return filiais.map(filial => ({ key: filial._id, value: filial._id, text: filial.nomeFilial.toUpperCase() }))
  }, [filiais])

  const handleFormChange = (name) => (event) => {
    setUser({ ...user, [name]: event.target.value });
  };

  return (
    <>
      <div
        style={{
          position: "fixed",
          height: "100vw",
          width: "100vw",
          background: "#00a688",
        }}
      ></div>
      <div style={{ height: "100vh" }}>
        <Grid
          textAlign="center"
          style={{ height: "100%" }}
          verticalAlign="middle"
          centered
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h1" textAlign="center">
              <Image
                style={{ height: "10em", width: "100%", objectFit: "contain" }}
                src={`${process.env.PUBLIC_URL}/logo.png`}
              />
            </Header>
            <Form error={erro !== null} loading={loading} size="large">
              <Segment stacked padded>                
                { filialContext.isFilial && <Form.Select
                  label="Filial"
                  name="filial"
                  options={optionsFilial}
                  value={user.filial}
                  onChange={(e,target) => handleFormChange("filial")({ target })}
                  fluid
                  placeholder="Selecione a filial"
                  
                />}
                <Form.Input
                  value={user.username}
                  onChange={handleFormChange("username")}
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder="Usuário"
                />
                <Form.Input
                  value={user.password}
                  onChange={handleFormChange("password")}
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Senha"
                  type="password"
                />
                <Message error content="Usuário ou senha inválidos" />
                <Button
                  color={MAIN_COLOR}
                  disabled={loading}
                  loading={loading}
                  fluid
                  size="large"
                  onClick={signin}
                >
                  Entrar
                </Button>
                <a href="#" style={{ float: "right" }}>
                  Esqueceu sua senha?
                </a>
                <div style={{ clear: "both" }} />
              </Segment>
            </Form>
          </Grid.Column>
        </Grid>
      </div>
    </>
  );
};

export default withRouter(LoginForm);
