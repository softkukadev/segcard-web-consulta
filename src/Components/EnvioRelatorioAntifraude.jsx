import React, { useState } from 'react'
import { Form, Header, Input, List, Segment } from 'semantic-ui-react'
import Api from '../Api'
import { MAIN_COLOR } from '../environment'

const EnvioRelatorioAntifraude = props => {
    const [file, setFile] = useState('')
    const [loading, setLoading] = useState(false);
    const [retorno, setRetorno] = useState([]);

    const onChangeFile = (files) =>  {
        if(files && files[0]) {
            setFile(files[0])
        }
    }

    const submit = async () => {
        if(file) {
            setLoading(true)
            try {
                const form = new FormData()
                form.append('file', file);
                const ret = await Api.Sell.cancelamento_lote(form);
                setRetorno(ret);
            } catch(err) {
                setRetorno([]);
            } finally{
                setLoading(false)
            }
        }
    }


    return <>
        <Segment loading={loading}>
            <Header content="Envio de relatório antifraude" as="h3" dividing />
            <Form>
                <Form.Field>
                    <label>Selecione o arquivo</label>
                    <Input name="file" type="file" style={{width: '100%'}} onChange={(event) => onChangeFile(event.target.files)} accept=".xls,.xlsx" />
                </Form.Field>
                <Form.Button disabled={!file} loading={loading} onClick={submit} content="Enviar" fluid color={MAIN_COLOR} />
            </Form>

            {retorno.length > 0 && <>
                <Header color="red" content="Retorno" />
                <List divided>
                    {retorno.map((ret, i) => {
                        return  <List.Item key={i}>{ret}</List.Item>
                    })}
                </List>
            </>}
        </Segment>
    </>
}

export default EnvioRelatorioAntifraude