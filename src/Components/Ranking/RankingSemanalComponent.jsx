import React, { useState, useEffect, useCallback } from "react";
import styled from "styled-components";
import Api from "../../Api";
import { List, Header, Icon } from "semantic-ui-react";

const RankingSemanalComponent = (props) => {
  const { refresh, dataInicio, dataFim } = props;
  const [ranking, setRanking] = useState([]);
  const [loading, setLoading] = useState(false);

  const load = useCallback(async (dataInicio, dataFim, cancel) => {
    setLoading(true);
    try {
      const data = await Api.Sell.rankingSemanal(dataInicio?.toISOString(), dataFim?.toISOString(), cancel);
      setRanking(data);
    } finally {
      setLoading(false);
    }
  }, [])

  useEffect(() => {
    const cancelToken = Api.Sell.getCancelToken();
    load(dataInicio, dataFim, cancelToken);
  }, [refresh, dataInicio, dataFim]);

  
  if(loading) {
    return <div style={{margin: '0 auto', display: 'flex', alignItems: "center", justifyContent: 'center', width: '100%', height: '100%'}}>
      <Icon name="refresh" loading size="huge" />
    </div>
  }

  return (
    <List ordered divided size="massive" relaxed verticalAlign="middle">
      {ranking.filter(a => a.aprovadas > 0).map((rank, i) => (
        <List.Item>
          <Header as="h1" floated="left" style={{marginLeft: 10}} icon={i === 0 ? "trophy" : null} content={rank._id.operator_name?.toUpperCase()} />
          <Header floated="right" as="h1">
              <span style={{color:  'green'}}>{rank.aprovadas}</span>
          </Header>
        </List.Item>
      ))}
    </List>
  );
};

export default RankingSemanalComponent;
