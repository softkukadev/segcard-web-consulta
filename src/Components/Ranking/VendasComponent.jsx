import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Api from "../../Api";
import { List, Header, Loader, Container, Icon } from "semantic-ui-react";
import Moment from "react-moment";

const VendasComponent = (props) => {
  const { refresh} = props;
  const [vendas, setVendas] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function load(cancel) {
      setLoading(true);
      try {
        const { docs } = await Api.Sell.list_sells(cancel, 1, {});
        setVendas(docs);
      } finally {
        setLoading(false);
      }
    }
    const cancelToken = Api.Sell.getCancelToken();
    load(cancelToken);
  }, [refresh]);

  if(loading) {
    return <div style={{margin: '0 auto', display: 'flex', alignItems: "center", justifyContent: 'center', width: '100%', height: '100%'}}>
      <Icon name="refresh" loading size="huge" />
    </div>
  }

  return (
      <List divided size="huge" relaxed verticalAlign="middle">
        {vendas.map((venda) => (
          <List.Item>
            <Header as="h4" floated="left" style={{marginLeft: 10}} content={venda.operador?.name?.toUpperCase()} />
            <Header floated="right" as="h4">
                <Moment format="DD/MM/YYYY HH:mm">{venda.created_at}</Moment>
            </Header>
          </List.Item>
        ))}
      </List>
  );
};

export default VendasComponent;
