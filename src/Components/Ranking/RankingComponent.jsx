import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Api from "../../Api";
import { List, Header, Icon } from "semantic-ui-react";

const RankingComponent = (props) => {
    const { refresh} = props;
  const [ranking, setRanking] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function load(cancel) {
      setLoading(true);
      try {
        const data = await Api.Sell.ranking(cancel);
        setRanking(data);
      } finally {
        setLoading(false);
      }
    }
    const cancelToken = Api.Sell.getCancelToken();
    load(cancelToken);
  }, [refresh]);

  
  if(loading) {
    return <div style={{margin: '0 auto', display: 'flex', alignItems: "center", justifyContent: 'center', width: '100%', height: '100%'}}>
      <Icon name="refresh" loading size="huge" />
    </div>
  }

  return (
    <List ordered divided size="huge" relaxed verticalAlign="middle">
      {ranking.filter(a => a.aprovadas > 0).map((rank) => (
        <List.Item>
          <Header as="h4" floated="left" style={{marginLeft: 10}} content={rank._id.operator_name?.toUpperCase()} />
          <Header floated="right" as="h4">
              <span style={{color:  'green'}}>{rank.aprovadas}</span>
          </Header>
        </List.Item>
      ))}
    </List>
  );
};

export default RankingComponent;
