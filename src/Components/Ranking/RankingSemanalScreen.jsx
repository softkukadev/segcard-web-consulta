import React, { useMemo, useState } from 'react';
import styled from 'styled-components';
import { Divider, Form, Header } from 'semantic-ui-react';
import { useTime } from 'use-time-react-hook';
import RankingSemanalComponent from './RankingSemanalComponent';
import moment from 'moment'
import { DatesRangeInput } from 'semantic-ui-calendar-react';

const Background = styled.div`
    background: #e2e1e0;
    width: 100vw;
    height: 100vh;
    padding: 2em;
`

const Logo = styled.div`
    background: url(${process.env.PUBLIC_URL}/logo-alt.png);
    background-size: contain;
    height: ${props => props.height ?? 60}px;
    width: auto;
    background-repeat: no-repeat;
    background-position: top center;
`

const Separator = styled.div`
    background: #00a688;
    height: 5px;
    width: 100%;
    margin: 10px auto;

`

const Card = styled.div`
    padding: 1rem;
    background: #FCFCFC;
    border-radius: 2px;
    width: 100%;
    overflow-y: auto;
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    transition: all 0.3s cubic-bezier(.25,.8,.25,1);

    &:hover {
        box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-areas: 'ranking';
    grid-template-rows: 100%;
    height: calc(100% - 90px);
    gap: 1rem;
`

const RankingSemanalScreen = props => {
    const [time] = useTime({ interval: '60 sec' })
    const [periodo, setPeriodo] = useState(`${moment().format('DD/MM/YYYY')} - ${moment().format('DD/MM/YYYY')}`);

    const { dataInicio, dataFim } = useMemo(() => {
        const [ini, fim] = periodo.split(' - ');
        if(ini.trim() && fim.trim()){
            const  dataInicio = moment(ini, 'DD/MM/YYYY').startOf('day').toDate()
            const  dataFim = moment(fim, 'DD/MM/YYYY').endOf('day').toDate()
            return { dataInicio, dataFim }
        }
        return { dataInicio: moment().startOf('day').toDate(), dataFim: moment().endOf('day').toDate()}
    }, [periodo])

    return <Background>
        <Logo height={60} />
        <Separator />
        <Grid>
            <Card style={{gridArea: 'ranking'}}>
                <span style={{display: 'flex'}}>
                    <Header style={{flex: 1}} as="h1" content="Ranking Semanal" subheader="Este ranking compreende informações de vendas de segunda a sexta apenas em horário comercial." />
                    <div style={{width: 300}}>
                        <Form>
                            <Form.Field>
                                <label>Período</label>
                                <DatesRangeInput localization="pt-BR" allowSameEndDate clearable dateFormat="DD/MM/YYYY" closable  value={periodo} onChange={(_, { value}) => setPeriodo(value)}></DatesRangeInput>
                            </Form.Field>
                        </Form>
                    </div>
                </span>
                <Divider/>
                <RankingSemanalComponent refresh={time} dataInicio={dataInicio} dataFim={dataFim}  />
            </Card>
        </Grid>
        
    </Background>
}

export default RankingSemanalScreen;