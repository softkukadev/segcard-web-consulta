import React from 'react';
import styled from 'styled-components';
import { Header } from 'semantic-ui-react';
import RankingComponent from './RankingComponent';
import VendasComponent from './VendasComponent';
import MetasComponent from './MetasComponent';
import { useTime } from 'use-time-react-hook';

const Background = styled.div`
    background: #e2e1e0;
    width: 100vw;
    height: 100vh;
    padding: 2em;
`

const Logo = styled.div`
    background: url(${process.env.PUBLIC_URL}/logo-alt.png);
    background-size: contain;
    height: ${props => props.height ?? 60}px;
    width: auto;
    background-repeat: no-repeat;
    background-position: top center;
`

const Separator = styled.div`
    background: #00a688;
    height: 5px;
    width: 100%;
    margin: 10px auto;

`

const Card = styled.div`
    padding: 1rem;
    background: #FCFCFC;
    border-radius: 2px;
    width: 100%;
    overflow-y: auto;
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    transition: all 0.3s cubic-bezier(.25,.8,.25,1);

    &:hover {
        box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-areas: 'metas metas'
                         'vendas ranking';
    grid-template-rows: 350px calc(100% - 350px);
    grid-template-columns: 50% 50%;
    height: calc(100% - 90px);
    gap: 1rem;
`

const RankingScreen = props => {
    const [time] = useTime({ interval: '30 sec' })
    return <Background>
        <Logo height={60} />
        <Separator />
        <Grid>
           
            <Card style={{gridArea: 'metas'}}>
                <Header dividing as="h2" content="Metas" />
                <MetasComponent refresh={time} />
            </Card>

            <Card style={{gridArea: 'vendas'}}>
                <Header dividing as="h2" content="Ultimas vendas" />
                <VendasComponent refresh={time} />
            </Card> 

            <Card style={{gridArea: 'ranking'}}>
                <Header dividing as="h2" content="Ranking" />
                <RankingComponent refresh={time} />
            </Card>
        </Grid>
        
    </Background>
}

export default RankingScreen;