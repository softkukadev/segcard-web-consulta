import React, { useState, useEffect, useMemo } from "react";
import {
  Grid,
  Segment,
  Header,
  Statistic,
  Placeholder,
  Icon,
} from "semantic-ui-react";
import { MAIN_COLOR } from "../../environment";
import Api from "../../Api";
import { ResponsiveContainer, PieChart, Pie, Cell, Legend } from "recharts";

const EstatisticaBlock = (props) => {
  const { loading } = props;

  if (loading) {
    return (
      <Segment textAlign="center" attached stacked padded color={props.color}>
        <Placeholder>
          <Placeholder.Header></Placeholder.Header>
          <Placeholder.Line></Placeholder.Line>
        </Placeholder>
      </Segment>
    );
  }

  return (
    <Segment textAlign="center" attached stacked padded color={props.color}>
      <Statistic value={props.value || 0} label={props.title} />
    </Segment>
  );
};

const MetasComponent = (props) => {
  const { refresh } = props;
  const [loading, setLoading] = useState(false);
  const [estatisticas, setEstatisticas] = useState({});
  const [estatisticasDia, setEstatisticasDia] = useState({});
  const [meta, setMeta] = useState(1000)

  const load = async (cancel) => {
    setLoading(true);
    try {
      const estatisticas = await Api.Sell.estatisticas(cancel);
      setEstatisticas(estatisticas);
      const estatisticasDia = await Api.Sell.estatisticas_dia(cancel);
      setEstatisticasDia(estatisticasDia);
      const meta_mensal = await Api.Provider.getParamentro('meta_mensal');
      setMeta(+meta_mensal.value);
    } catch (erro) {}
    setLoading(false);
  };

  useEffect(() => {
    const cancel = Api.Sell.getCancelToken();
    load(cancel);
    return () => cancel.cancel();
  }, [refresh]);

  const chartData = useMemo(() => {
    const data = [];

    if(estatisticasDia) {
      data.push({ total: estatisticasDia.aprovadas,  tipo: 'Aprovadas hoje', color: '#98eb34' })
    }

    if(estatisticas) {
      data.push({ total: (estatisticas.aprovadas - (estatisticasDia?.aprovadas || 0)),  tipo: 'Aprovadas mês', color: '#34aeeb' })
      data.push({ total: estatisticas.canceladas,  tipo: 'Canceladas mês', color: '#b02836' })
    }    
    
    if(meta) {
      const restanteMeta = meta - (estatisticas?.aprovadas || 0);
      if(restanteMeta > 0) {
        data.push({ total: restanteMeta,  tipo: 'Restante meta', color: 'black' })
      }
    }
    return data
  }, [estatisticas, estatisticasDia, meta])

  if(loading) {
    return <div style={{margin: '0 auto', display: 'flex', alignItems: "center", justifyContent: 'center', width: '100%', height: '100%'}}>
      <Icon name="refresh" loading size="huge" />
    </div>
  }

  return (
    <Grid verticalAlign="middle" style={{height: '90%'}}>
      <Grid.Column width={12} >
        <Grid verticalAlign="middle">
          <Grid.Row columns={1}>
            <Grid.Column>
              <Header as="h1" style={{fontSize: '3em'}}>Meta mensal: {meta}</Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={1}>
            <Grid.Column>
              <Header as="h1" style={{fontSize: '3em'}}>Vendas no mês: {estatisticas.aprovadas}</Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={1}>
            <Grid.Column>
              <Header as="h1" style={{fontSize: '3em'}}>
                Vendas no dia: {estatisticasDia.aprovadas}
              </Header>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Grid.Column>
      <Grid.Column width={4}>
        <ResponsiveContainer width={300} height={250} >
          <PieChart>
            <Pie data={chartData}  cx="50%" cy="50%" dataKey="total" nameKey="tipo" outerRadius={80} innerRadius={60} fill="#8884d8" label paddingAngle={2}>
              {chartData.map(cell => <Cell fill={cell.color} />)}
            </Pie>
            <Legend />
          </PieChart>
        </ResponsiveContainer>
      </Grid.Column>
    </Grid>
  );
};

export default MetasComponent;
