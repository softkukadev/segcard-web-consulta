import React, { useState, useEffect, useRef } from "react";
import { Label, Button } from "semantic-ui-react";
import PropTypes from 'prop-types';
import moment from 'moment';
import "react-dates/initialize";
import { DayPickerRangeController } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import styled from 'styled-components';
import Moment from "react-moment";
import { MAIN_COLOR } from "../environment";

/**
 * @author
 * @function FiltroDatas
 **/

const WrapperCalendar = styled.div`
  background: white;
  border: 1px solid ${MAIN_COLOR.hex};
  border-radius: 5px;
  position: absolute;
  z-index: 99;
  padding: .3em;
  margin-top: 3px;
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
`

function useOutsideClick(ref, callback){
  function handleClickOutside(event) {
    if (ref.current && !ref.current.contains(event.target)) {
      callback();
    }
  }

  useEffect(() => {
    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  });
}
 

const FiltroDatas = props => {
  const { periodo, setPeriodo, callback } = props;
  const [active, setActive] = useState(null);
  const [foco, setFoco] = useState('startDate');

  const [openDateSelect, setOpenDateSelect] = useState(false);
  let ref = useRef(null);
  useOutsideClick(ref, () => setOpenDateSelect(false));

  const activateFilter = index => {
    let datas = {}
    switch (index){
      case 1:
        datas = {
          startDate: moment().startOf('day'),
          endDate: moment().endOf('day'),
          option: 1
        };
        break;
      case 2:
        datas = {
          startDate: moment().add(-1, 'day').startOf('day'),
          endDate: moment().add(-1, 'day').endOf('day'),
          option: 2
        };
        break;
      case 3:
        datas = {
          startDate: moment().add(-7, 'days').startOf('day'),
          endDate: moment().endOf('day'),
          option: 3
        };
        break;
      case 4:
        datas = {
          startDate: moment().add(-15, 'days').startOf('day'),
          endDate: moment().endOf('day'),
          option: 4
        };
        break;
      case 5:
        datas = {
          startDate: moment().add(-30, 'days').startOf('day'),
          endDate: moment().endOf('day'),
          option: 5
        };
        break;
      case 6:
        datas = {
          startDate: moment().add(-90, 'days').startOf('day'),
          endDate: moment().endOf('day'),
          option: 6
        };
        break;
      default:
        return null
    }
    setPeriodo(datas);
    callback(datas.startDate, datas.endDate);
    setActive(index);
  }

  useEffect(() => {
    if(periodo.option){
      setActive(periodo.option)
    }
    return () => {
      setActive(null)
    };
  }, [periodo.option])

  return (
      <Label.Group size="medium">
        <Label basic={active !== 1} content="Hoje" as="a" onClick={() => activateFilter(1)} color={MAIN_COLOR} />
        <Label basic={active !== 2} content="Ontem" as="a" onClick={() => activateFilter(2)} color={MAIN_COLOR} />
        <Label basic={active !== 3} content="Últimos 7 dias" as="a" onClick={() => activateFilter(3)} color={MAIN_COLOR} />
        <Label basic={active !== 4} content="Últimos 15 dias" as="a" onClick={() => activateFilter(4)} color={MAIN_COLOR} />
        <Label basic={active !== 5} content="Últimos 30 dias" as="a" onClick={() => activateFilter(5)} color={MAIN_COLOR} />
        <Label basic={active !== 6} content="Últimos 3 meses" as="a" onClick={() => activateFilter(6)} color={MAIN_COLOR} />
        <div className="ui" style={{display: 'inline-block'}}>
          <Label basic={active !== 7} as="a" onClick={() => setOpenDateSelect(!openDateSelect)} color={MAIN_COLOR}>
            {active !== 7 && "Personalizado"}
            {active === 7 && (
              <>
                <Moment format="DD/MM/YYYY">{periodo.startDate}</Moment>
                &nbsp;-&nbsp;
                <Moment format="DD/MM/YYYY">{periodo.endDate}</Moment>
              </>
            )}
          </Label>
          { openDateSelect &&
            <WrapperCalendar ref={ref}>
              <Button icon="close" floated="right" size="tiny" basic onClick={() => setOpenDateSelect(false)} />
              <div style={{clear: 'both'}}/>
              <div style={{margin: 2}}>
                <DayPickerRangeController
                  noBorder
                  startDate={periodo.startDate} // momentPropTypes.momentObj or null,
                  numberOfMonths={2}
                  minimumNights={0}
                  endDate={periodo.endDate} // momentPropTypes.momentObj or null,
                  onDatesChange={setPeriodo} // PropTypes.func.isRequired,
                  focusedInput={foco} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                  onFocusChange={focusedInput => setFoco(focusedInput || 'startDate')} // PropTypes.func.isRequired,
                  //onClose={({ startDate, endDate }) => load(startDate, endDate)}
                />
              </div>
              <Button content="Aplicar" icon="check" disabled={!(periodo.startDate && periodo.endDate)} onClick={() => {setActive(7); setOpenDateSelect(false); callback(periodo.startDate, periodo.endDate)}} floated="right" color={MAIN_COLOR} />
              <Button content="Limpar" icon="close" onClick={() => {setActive(null); setOpenDateSelect(false); setPeriodo({startDate: null, endDate: null})}} basic floated="right" />
            </WrapperCalendar>
          }
          {
            periodo.startDate && periodo.endDate &&
            <Button size="tiny" circular icon="close" content="Limpar filtro" color={MAIN_COLOR} compact onClick={() => {setActive(null); setOpenDateSelect(false); setPeriodo({startDate: null, endDate: null})}} />
          }
        </div>
      </Label.Group>
  );
};

FiltroDatas.propTypes = {
  periodo: PropTypes.object.isRequired,
  setPeriodo: PropTypes.func.isRequired
}

export default FiltroDatas;
