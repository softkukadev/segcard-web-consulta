import React from 'react'
import { connect } from 'react-redux'
import { Header } from 'semantic-ui-react';
import NovaVenda from '../NovaVenda';


const Inicio = props => {
    return (
      <>
        <NovaVenda {...props} />
      </>
    )
}


const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(Inicio)