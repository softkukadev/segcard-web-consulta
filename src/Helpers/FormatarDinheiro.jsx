import React from 'react'
import PropTypes from 'prop-types'
import NumberFormat from 'react-number-format';

const FormatarDinheiro = ({value, tipo, onChange, isNumericString, placeholder}) => {
    return(
        <NumberFormat placeholder={placeholder} thousandSeparator="." decimalSeparator="," value={value} displayType={tipo} onValueChange={(valor) => typeof onChange === 'function' && (!isNaN(valor.floatValue) && onChange(valor.floatValue))} fixedDecimalScale={true} decimalScale={2} prefix="R$ " />
    );
}

FormatarDinheiro.propTypes = {
    value: PropTypes.any.isRequired,
    tipo: PropTypes.oneOf(['text', 'input']),
    isNumericString: PropTypes.bool,
    onChange: PropTypes.func,
    placeholder: PropTypes.string
}

export default FormatarDinheiro;