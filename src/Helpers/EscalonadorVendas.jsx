import React, { useState, useEffect, useMemo, useCallback } from 'react'
import { Button, Divider, Form, Header, Segment } from 'semantic-ui-react'
import Api from '../Api'
import FormatarDinheiro from './FormatarDinheiro'

const EscalonadorVendas = props => {
    const [regras, setRegras] = useState([])

    const adicionarRegra = useCallback(() => {
        setRegras(regras => {
            if(regras.length > 0) {
                const ultimaRegra = regras[regras.length - 1];
                return [...regras, { min: ultimaRegra.max, max: ultimaRegra.max, valor: '' }]    
            }
            return [...regras, { min: 0, max: 0, valor: '' }]
        })
    }, [])

    const loadRules = useCallback(async () => {
        try {
            const rules = await Api.Payroll.getRules()
            setRegras(rules.rules)
        } catch(err) {
            setRegras([])
        }
    }, [])

    useEffect(() => { 
        loadRules()
    }, [])

    const submitRules = useCallback(async (rules) => {
        try {
            const rulesSaved = await Api.Payroll.updateRules(rules);
            window.alert("Regras salvas com sucesso!");
        } catch(error) {
            window.alert("Erro ao salvar regras");
        }
    }, [])
    
    const onChangeRegra = useCallback((index, target) => {
        setRegras(regras => {
            const regra = regras[index];
            if(regra) {
                regra[target.name] = target.value
            }
            return [...regras]
        })
    }, [])

    const removerRegra = useCallback((index) => {
        setRegras(regras => {
            const newRules = [...regras]
            newRules.splice(index, 1);
            return newRules
        })
    })

    return <>
        <Header as="h2">Escalonagem de vendas</Header>
        <Divider />
        <Form onSubmit={() => submitRules(regras)}>
            { regras.map((regra, indice) => (
                <React.Fragment key={indice}>
                    <Form.Group>
                        <Form.Field width={5}>
                            <label>Minimo de vendas</label>
                            <Form.Input value={regra.min} name="min" onChange={(e, t) => onChangeRegra(indice, t)} type="number"></Form.Input>
                        </Form.Field>
                        <Form.Field width={5}>
                            <label>Maximo de vendas</label>
                            <Form.Input min={indice > 0 ? regras[indice-1]?.max : 0} value={regra.max} name="max" onChange={(e, t) => onChangeRegra(indice, t)} type="number"></Form.Input>
                        </Form.Field>
                        <Form.Field width={5}>
                            <label>Valor por venda</label>
                            <FormatarDinheiro value={regra.valor} name="valor" onChange={(valor) => onChangeRegra(indice, {name: 'valor', value: valor})} tipo="input" />
                        </Form.Field>
                        <Form.Field width={1}>
                            <label>Remover</label>
                            <Button type="button" fluid icon="trash" color="red" onClick={() => removerRegra(indice)}></Button>
                        </Form.Field>
                    </Form.Group>
                </React.Fragment>
                )
            )}
            <Button type="button" fluid color="black" basic content="Adicionar regra" icon="add circle" onClick={adicionarRegra}></Button>
            <Divider />
            <Button fluid color="primary" content="Salvar Regras" icon="save"></Button>
        </Form>
    </>
}

export default EscalonadorVendas