import csv from "csvtojson";

const leituraCsvLote = async (csvContent) => {
  const data = await csv({
    output: "json",
    trim: true,
    delimiter: [";", ","],
  }).fromString(csvContent);
  return data;
};

export function renameFields(arr, fields) {
  if (Array.isArray(arr)) {
    const fieldsArray = Object.keys(fields);
    const ret = arr.map((item) => {
      const data = fieldsArray.reduce((obj, field) => {
        const new_field_name = fields[field];
        return { ...obj, [new_field_name]: item[field] };
      }, {});
      return data;
    });
    return ret;
  }
  throw new Error("Dados devem ser um array");
}

export default leituraCsvLote;
