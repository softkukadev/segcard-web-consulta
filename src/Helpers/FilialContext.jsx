import React from 'react'

const FilialContext = React.createContext({
    isFilial: false,
    id_filial: null
})

export default FilialContext;