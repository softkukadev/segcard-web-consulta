import api from './api'
import Axios from 'axios'
import store from '../Store'
import ActionTypes from '../Store/ActionTypes'
import jwt_decode from 'jwt-decode'

export default class Payroll {
    
    static getCancelToken = () => Axios.CancelToken.source()

    static updateRules = async (rules) => {
        const req = await api.post('/payroll/rules', { rules })
        return req.data
    }

    static getRules = async () => {
        const req = await api.get('/payroll/rules');
        return req.data
    }
    
    static vendasPorOperador = async (current_month) => {
        const req = await api.get('/payroll/vendas_por_operador', {
            params:{
                current_month
            }
        });
        return req.data
    }

    static getFaltas = async () => {
        const req = await api.get('/payroll/faltas');
        return req.data
    }

    static registrarFalta = async (dados) => {
        const req = await api.post('/payroll/faltas', dados);
        return req.data
    }

    static confirmarFolha = async (dados) => {
        const req = await api.post('/payroll/confirmar_folha', dados);
        return req.data
    }

    static apagarFalta = async (id) => {
        const req = await api.delete('/payroll/faltas/'+id);
        return req.data
    }

}