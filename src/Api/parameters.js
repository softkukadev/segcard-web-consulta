import api from './api'
import Axios from 'axios'

export default class Parameters {
    
    static getCancelToken = () => Axios.CancelToken.source()

    static get = async () => {
        const req = await api.get('/parameters')
        return req.data
    }

    static update_provider = async (id, data) => {
        const req = await api.put(`/parameters/${id}`, data)
        return req.data
    }
    

}