import Axios from "axios";
import store from "./../Store";

// const baseURL = 'https://conex.softkuka.com.br/api'
//const baseURL = 'http://192.168.1.55:4004'
// export const baseURL = "http://localhost:4004/api";
const baseURL = "https://cobranca.segcard.com/api";
// const baseURL = 'http://docsdigitaisauxiliadora.com:8092'
// const baseURL = "/api";

const api = Axios.create({
  baseURL,
});

api.interceptors.request.use(async (config) => {
  if (store.getState().auth) {
    const { access_token } = store.getState().auth;
    config.headers.authorization = `Bearer ${access_token}`;
  }
  return config;
});

export default api;
