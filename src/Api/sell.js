import Axios from "axios";
import api from "./api";

export default class Sell {
  static getCancelToken = () => Axios.CancelToken.source();

  static list_sells = async (
    cancelToken,
    page,
    filtros,
    exportar = undefined
  ) => {
    const req = await api.get(`/sales`, {
      params: {
        page,
        ...filtros,
        exportar,
      },
      cancelToken: cancelToken.token,
    });
    return req.data;
  };

  static sale_por_id = async (id, cancelToken) => {
    const req = await api.get(`/sales/${id}`, {
      cancelToken: cancelToken.token,
    });
    return req.data;
  };

  static documentos_assinatura = async (id, cancelToken) => {
    const req = await api.get(`/documentos/assinatura/${id}`, {
      cancelToken: cancelToken.token,
    });
    return req.data;
  };

  static get_link_documento = async (id,) => {
    const req = await api.get(`/documentos/${id}`);
    return req.data;
  };

  static pagamentos_por_id = async (id, cancelToken) => {
    const req = await api.get(`/sales/${id}/pagamentos`, {
      cancelToken: cancelToken.token,
    });
    return req.data;
  };

  static pagamentos_por_data = async (dataInicio, dataFim, filtros, cancelToken) => {
    const req = await api.get(`/sales/pagamentos`, {
      params: {
        dataInicio: dataInicio.toISOString(),
        dataFim: dataFim.toISOString(),
        ...filtros
      },
      cancelToken: cancelToken.token,
    });
    return req.data;
  };

  static pagamentos_calendario = async (dataInicio, dataFim, cancelToken) => {
    const req = await api.get(`/sales/pagamentos_calendario`, {
      params: {
        dataInicio: dataInicio.toISOString(),
        dataFim: dataFim.toISOString()
      },
      cancelToken: cancelToken.token,
    });
    return req.data;
  };

  static failed = async (cancelToken, page, filtros, exportar = undefined) => {
    const req = await api.get(`/failedsales`, {
      params: {
        page,
        ...filtros,
        exportar,
      },
      cancelToken: cancelToken.token,
    });
    return req.data;
  };

  static new_sale = async (provider, data) => {
    const req = await api.post(`/sales/${provider}`, data);
    return req.data;
  };

  static estornar_pagamento = async (provider, id) => {
    const req = await api.post(`/sales/${provider}/estorno-pagamento/${id}`, {});
    return req.data;
  };

  static new_sale_array = async (provider, data) => {
    const req = await api.post(`/sales/${provider}/array`, data);
    return req.data;
  };

  static cancel_sale = async (provider, id) => {
    const req = await api.post(`/subscriptions/${provider}/${id}/unsubscribe`);
    return req.data;
  };

  static ranking = async (cancel) => {
    const req = await api.get(`/sales/ranking`, {
      cancelToken: cancel.token,
    });
    return req.data;
  };

  static rankingSemanal = async (dataInicio, dataFim, cancel) => {
    const req = await api.get(`/sales/ranking-semanal`, {
      params: {
        dataInicio,
        dataFim
      },
      cancelToken: cancel.token,
    });
    return req.data;
  };

  static rankingDiario = async (cancel) => {
    const req = await api.get(`/sales/ranking-diario`, {
      cancelToken: cancel.token,
    });
    return req.data;
  };

  static estatisticas = async (cancel) => {
    const req = await api.get(`/sales/stats`, {
      cancelToken: cancel.token,
    });
    return req.data;
  };

  static estatisticas_dia = async (cancel) => {
    const req = await api.get(`/sales/stats/dia`, {
      cancelToken: cancel.token,
    });
    return req.data;
  };

  static estatisticas_dia_mes = async (cancel, porFilial) => {
    const params = {}
    if(porFilial) {
      params.porFilial = true;
    }
    const req = await api.get(`/sales/stats/dia-mes`, {
      params,
      cancelToken: cancel.token,
    });
    return req.data;
  };

  static cancelamento_lote = async (formdata) => {
    const req = await api.post(`/sales/lote-possivel-fraude`, formdata);
    return req.data;
  };
}
