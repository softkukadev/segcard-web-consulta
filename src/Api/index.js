import Auth from './auth'
import Provider from './provider'
import Sell from './sell'
import Parameters from './parameters'
import Filial from './filial'
import Seguradora from './seguradora'
import Payroll from './payroll'
import UsuariosAplicativo from './usuarios-aplicativo'

export default { Auth, Provider, Sell, Parameters, Filial, Seguradora, Payroll, UsuariosAplicativo }