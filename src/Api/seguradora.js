import api from './api'
import Axios from 'axios'
import store from './../Store'
import ActionTypes from '../Store/ActionTypes'

export default class Seguradora {
    
    static getCancelToken = () => Axios.CancelToken.source()

    static list_seguradoras = async (cancelToken) => {
        const req = await api.get('/seguradoras',{
            cancelToken: cancelToken.token
        })
        return req.data
    }
    
    static set_default = async (id) => {
        const req = await api.put(`/seguradoras/default/${id}`)
        return req.data
    }

    static update_seguradora = async (id, data) => {
        const req = await api.put(`/seguradoras/${id}`, data)
        return req.data
    }

    static cancelar_seguro = async (id, seguradora) => {
        const req = await api.delete(`/seguradoras/cancelar_apolice/${id}/${seguradora}`)
        return req.data
    }
    

}