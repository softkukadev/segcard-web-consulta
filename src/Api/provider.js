import api from './api'
import Axios from 'axios'
import store from './../Store'
import ActionTypes from '../Store/ActionTypes'

export default class Provider {
    
    static getCancelToken = () => Axios.CancelToken.source()

    static get_current = async () => {
        const req = await api.get('/providers/default')
        const valorPlano = await api.get('/parameters/valor_plano')
        const vp = valorPlano.data

        const provider = {
            ...req.data,
            valor_plano: +vp.value
        }

        store.dispatch({ type: ActionTypes.PROVIDER.LOAD_DEFAULT, payload: provider })
        return req.data
    }

    static getParamentro = async (nome) => {
        const parametro = await api.get(`/parameters/${nome}`)
        return parametro.data
    }

    static list_providers = async (cancelToken) => {
        const req = await api.get('/providers',{
            cancelToken: cancelToken.token
        })
        return req.data
    }
    
    static set_default = async (id) => {
        const req = await api.put(`/providers/default/${id}`)
        return req.data
    }
    
    static set_default_recorrencia = async (id) => {
        const req = await api.put(`/providers/default_recorrencia/${id}`)
        return req.data
    }

    static update_provider = async (id, data) => {
        const req = await api.put(`/providers/${id}`, data)
        return req.data
    }
    

}