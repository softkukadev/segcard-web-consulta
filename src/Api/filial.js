import api from './api'
import Axios from 'axios'
import store from './../Store'
import ActionTypes from '../Store/ActionTypes'
import jwt_decode from 'jwt-decode'

export default class Filial {
    
    static getCancelToken = () => Axios.CancelToken.source()

    static register = async (data) => {
        const req = await api.post('/filial/register', data)
        return req.data
    }

    static alterar_provider = async (idFilial, data) => {
        const req = await api.put(`/filial/${idFilial}`, data)
        return req.data
    }

    static disable = async (id) => {
        const req = await api.put(`/filial/disable/${id}`)
        return req.data
    }

    static enable = async (id) => {
        const req = await api.put(`/filial/enable/${id}`)
        return req.data
    }

    static list_all = async (cancelToken, page, filter) => {
        const req = await api.get('/filial', {
            cancelToken: cancelToken.token
        })
        return req.data
    }

}