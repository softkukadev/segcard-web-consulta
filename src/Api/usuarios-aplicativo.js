import api from './api'
import Axios from 'axios'

export default class UsuariosAplicativo {
    
    static getCancelToken = () => Axios.CancelToken.source()

    static getUsuarios = async (page = 1) => {
        const req = await api.get('/usuarios/cadastrados', {
            params: {
                page
            }
        })
        return req.data
    }

    

}