export const LOGIN = "/login";
export const INICIO = "/";

export const ASSINATURAS = "/assinaturas";
export const CONFIGURATION = "/configuracoes";
export const USUARIOS = "/usuarios";
export const ENVIO = "/envio";
export const FALHAS = "/falhas";
export const LOTES = "/lotes";
export const VISUALIZAR_ASSINATURA = "/assinatura/:id";
export const RANKING = "/ranking";
export const RANKING_SEMANAL = "/ranking-semanal";
export const RECORRENCIAS = "/recorrencias";
export const CONTROLE_MENSAL = "/controle-mensal";
export const FILIAIS = "/filiais";
export const USUARIOS_FILIAIS = "/filiais/:id/usuarios";
export const RANKING_DIARIO = "/ranking-diario";
export const TABELA_MES_DIA = "/mes-dia";
export const CANCELAMENTO_LOTE = "/cancelamento-lote";
export const FOLHA_PAGAMENTO = "/folha-pagamento";
export const FALTAS = "/faltas";

export const PAINEL_APLICATIVO = "/painel-aplicativo";

export default {
  INICIO,
  LOGIN,
  ASSINATURAS,
  CONFIGURATION,
  USUARIOS,
  FALHAS,
  ENVIO,
  LOTES,
  VISUALIZAR_ASSINATURA,
  RANKING,
  RECORRENCIAS,
  FILIAIS,
  USUARIOS_FILIAIS,
  RANKING_DIARIO,
  CONTROLE_MENSAL,
  TABELA_MES_DIA,
  CANCELAMENTO_LOTE,
  FOLHA_PAGAMENTO,
  FALTAS,
  RANKING_SEMANAL,
  PAINEL_APLICATIVO
};
