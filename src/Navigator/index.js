import { createBrowserHistory } from "history";
import React from "react";
import { Route, Router } from "react-router-dom";
import Assinaturas from "../Components/Assinaturas";
import VisualizarAssinatura from "../Components/Assinaturas/VisualizarAssinatura/VisualizarAssinatura";
import Configuracoes from "../Components/Configuracoes";
import EnvioArquivos from "../Components/EnvioArquivos/EnvioArquivos";
import Falhas from "../Components/Falhas/Falhas";
import Inicio from "../Components/Inicio";
import Login from "../Components/Login";
import Usuarios from "../Components/Usuarios";
import Filiais from "../Components/Filiais";
import PrivateRoute from "./PrivateRoute";
import Routes from "./routes";
import RankingScreen from "../Components/Ranking/RankingScreen";
import ConsultaRecorrencias from "../Components/Recorrencias/ConsultaRecorrencias";
import FilialContext from "../Helpers/FilialContext";
import RankingDiarioScreen from "../Components/Ranking/RankingDiarioScreen";
import ControleMensal from "../Components/ControleMensal/ControleMensal";
import FolhaPagamento from "../Components/FolhaPagamento/FolhaPagamento";
import TabelaMesAno from "../Components/TabelaMesAno/TabelaMesAno";
import EnvioRelatorioAntifraude from "../Components/EnvioRelatorioAntifraude";
import RegistroFaltas from "../Components/RegistroFaltas/RegistroFaltas";
import RankingSemanalScreen from "../Components/Ranking/RankingSemanalScreen";
import PainelAplicativo from "../Components/PainelAplicativo/PainelAplicativo";

const history = createBrowserHistory();

export default function Navigator() {
  const subdomain = React.useMemo(() => {
    const full_url = window.location.hostname;
    return full_url.split('.')[0];
  })

  const filialContextData = React.useMemo(() => {
    return {
      isFilial: !['cob', 'cobranca'].includes(subdomain)
    }
  }, [subdomain])

  return (
    <FilialContext.Provider value={filialContextData}>
    <Router history={history}>
      <Route path={Routes.LOGIN} exact component={Login} />
      <Route path={Routes.ENVIO} exact component={EnvioArquivos} />
      <PrivateRoute path={Routes.INICIO} exact component={Inicio} />
      <PrivateRoute path={Routes.ASSINATURAS} exact component={Assinaturas} />
      <PrivateRoute
        path={Routes.CONFIGURATION}
        exact
        component={Configuracoes}
      />
      <PrivateRoute path={Routes.USUARIOS} exact component={Usuarios} />
      <PrivateRoute path={Routes.USUARIOS_FILIAIS} exact component={Usuarios} />
      <PrivateRoute path={Routes.FILIAIS} exact component={Filiais} />
      <PrivateRoute path={Routes.FALHAS} exact component={Falhas} />
      <PrivateRoute path={Routes.LOTES} exact component={EnvioArquivos} />
      <PrivateRoute path={Routes.RANKING} roles={['administrador']} fullscreen exact component={RankingScreen} />
      <PrivateRoute path={Routes.RANKING_DIARIO} roles={['administrador']} fullscreen exact component={RankingDiarioScreen} />
      <PrivateRoute path={Routes.RANKING_SEMANAL} roles={['administrador']} fullscreen exact component={RankingSemanalScreen} />
      <PrivateRoute path={Routes.RECORRENCIAS} roles={['administrador']} exact component={ConsultaRecorrencias} />
      <PrivateRoute path={Routes.CONTROLE_MENSAL} roles={['administrador']} exact component={ControleMensal} />
      <PrivateRoute path={Routes.TABELA_MES_DIA} roles={['administrador']} exact component={TabelaMesAno} />
      <PrivateRoute path={Routes.CANCELAMENTO_LOTE} roles={['administrador']} exact component={EnvioRelatorioAntifraude} />
      <PrivateRoute path={Routes.FOLHA_PAGAMENTO} roles={['administrador']} exact component={FolhaPagamento} />
      <PrivateRoute path={Routes.FALTAS} roles={['administrador']} exact component={RegistroFaltas} />
      <PrivateRoute path={Routes.VISUALIZAR_ASSINATURA} exact component={VisualizarAssinatura} />
      <PrivateRoute path={Routes.PAINEL_APLICATIVO}  component={PainelAplicativo} />
    </Router>
    </FilialContext.Provider>
  );
}
