import React from "react";
import { Route, Redirect } from "react-router-dom";
import jwt_decode from "jwt-decode";
import Menubar from "../Components/Menubar";
import { isFunction } from "util";
import { connect } from 'react-redux'


const PrivateRoute = ({ component: Component, fullscreen, auth: usuario, logout, fullWidth, permissions: roles, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        let authValid = false;

        let jwtToken;
        if(usuario){
          let auth = usuario !== null && usuario !== {};
          jwtToken = auth && jwt_decode(usuario.access_token);

          authValid = jwtToken && jwtToken.exp > (new Date().getTime() + 1) / 1000;
          
          if(Array.isArray(roles)) {
            authValid = authValid && roles.includes(jwtToken.role)
          }
          
          if (!authValid) {
            isFunction(logout) && logout();
          }
        }

        if(!authValid) {
          return <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location }
            }}
          />
        }
        
        return !fullscreen ? (
          <Menubar fullWidth={fullWidth ? true : false} currentRole={jwtToken && jwtToken.role}>
            <Component
              {...rest}
              {...props}
              currentRole={jwtToken}
            />
          </Menubar>
        ) : (
          <Component
              {...rest}
              {...props}
              currentRole={jwtToken}
            />
        );
      }}
    />
  );
};

const mapStateToProps = state => ({
  auth: state.auth
});


export default connect(mapStateToProps)(PrivateRoute);